.. index::
   pair: DocumentOrShadowRoot ; Web API
   ! DocumentOrShadowRoot

.. _DocumentOrShadowRoot_web_api:

================================
**DocumentOrShadowRoot** Web API
================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/HTMLObjectElement

.. toctree::
   :maxdepth: 5


   properties/properties
