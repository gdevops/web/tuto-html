
.. _DocumentOrShadowRoot_activeelement:

===========================================
DocumentOrShadowRoot.**activeElement**
===========================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/DocumentOrShadowRoot/activeElement

.. contents::
   :depth: 5


Definition
===============

The activeElement read-only property of the Document and ShadowRoot
interfaces returns the Element within the DOM or shadow DOM tree that
currently has focus. This property is inherited from the DocumentOrShadowRoot
mixin.

Often activeElement will return an <input> or <textarea> object if it
has the text selection at the time.

If so, you can get more detail by using the element's selectionStart
and selectionEnd properties.

Other times the focused element might be a <select> element (menu) or
an <input> element, of type "button", "checkbox", or "radio".

Typically a user can press the tab key to move the focus around the
page among focusable elements, and use the space bar to activate one
(that is, to press a button or toggle a radio button).

Which elements are focusable varies depending on the platform and the
browser's current configuration.

For example, on macOS systems, elements that aren't text input elements
are not typically focusable by default.


.. note:: Focus (which element is receiving user input events) is not
   the same thing as selection (the currently highlighted part of the
   document). You can get the current selection using window.getSelection().

When there is no selection, the active element is the page's **<body> or null**.

Call example
==============

::

    document.getElementById('id_description_iframe').contentDocument.activeElement.onblur = function()
    {
        // Le texte HTML issu du iframe id_description_iframe
        let description = document.getElementById('id_description_iframe').contentDocument.activeElement.innerHTML;
        console.log(`description=${description}`);
    }

Example
=========

HTML
-----

.. code-block:: html
   :linenos:


    <p>Select some text from one of the text areas below:</p>

    <form>
      <textarea name="ta-example-one" id="ta-example-one" rows="7" cols="40">This is Text Area One. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt, lorem a porttitor molestie, odio nibh iaculis libero, et accumsan nunc orci eu dui.</textarea>
      <textarea name="ta-example-two" id="ta-example-two" rows="7" cols="40">This is Text Area Two. Fusce ullamcorper, nisl ac porttitor adipiscing, urna orci egestas libero, ut accumsan orci lacus laoreet diam. Morbi sed euismod diam.</textarea>
    </form>

    <p>Active element ID: <b id="output-element"></b></p>
    <p>Selected text: <b id="output-text"></b></p>


Javascript
-------------

.. code-block:: javascript
   :linenos:

    function onMouseUp(e) {
      const activeTextarea = document.activeElement;
      const selection = activeTextarea.value.substring(
        activeTextarea.selectionStart, activeTextarea.selectionEnd
      );

      const outputElement = document.getElementById('output-element');
      const outputText = document.getElementById('output-text');
      outputElement.innerHTML = activeTextarea.id;
      outputText.innerHTML = selection;
    }

    const textarea1 = document.getElementById('ta-example-one');
    const textarea2 = document.getElementById('ta-example-two');
    textarea1.addEventListener('mouseup', onMouseUp, false);
    textarea2.addEventListener('mouseup', onMouseUp, false);
