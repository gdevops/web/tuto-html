
.. _DocumentOrShadowRoot_properties:

======================================
**DocumentOrShadowRoot** properties
======================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/HTMLObjectElement

.. toctree::
   :maxdepth: 5


   activeElement/activeElement
