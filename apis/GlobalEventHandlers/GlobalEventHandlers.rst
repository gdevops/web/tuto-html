.. index::
   pair: GlobalEventHandlers ; Web API
   ! GlobalEventHandlers

.. _GlobalEventHandlers_web_api:

=================================
**GlobalEventHandlers** Web API
=================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/GlobalEventHandlers

.. toctree::
   :maxdepth: 5

   definition/definition
   properties/properties
