

.. _GlobalEventHandlers_definition:

====================================
**GlobalEventHandlers** definition
====================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/GlobalEventHandlers




Definition
==========

The GlobalEventHandlers mixin describes the event handlers common to
several interfaces like HTMLElement, Document, or Window.

Each of these interfaces can, of course, add more event handlers in
addition to the ones listed below.

.. note:: GlobalEventHandlers is a mixin and not an interface; you can't
   actually create an object of type GlobalEventHandlers.
