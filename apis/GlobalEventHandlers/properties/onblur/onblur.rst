.. index::
   ! onblur

.. _GlobalEventHandlers_onblur:

==========================================
GlobalEventHandlers **onblur** property
==========================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/GlobalEventHandlers/onblur



Definition
===========


The onblur property of the GlobalEventHandlers mixin is the EventHandler
for processing blur events. It's available on Element, Document, and Window.

The blur event is raised when an element loses focus.


.. note:: The opposite of onblur is onfocus.


Syntax
=========

::

    target.onblur = functionRef;


Example
==========

This example uses onblur and onfocus to change the text within an <input> element.


HTML
-----

.. code-block:: html

    <input type="text" value="CLICK HERE">

JavaScript
------------

.. code-block:: javascript

    let input = document.querySelector('input');

    input.onblur = inputBlur;
    input.onfocus = inputFocus;

    function inputBlur() {
      input.value = 'Focus has been lost';
    }

    function inputFocus() {
      input.value = 'Focus is here';
    }
