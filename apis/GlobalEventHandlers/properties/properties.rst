
.. _GlobalEventHandlers_properties:

====================================
**GlobalEventHandlers** properties
====================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/GlobalEventHandlers

.. toctree::
   :maxdepth: 5

   onblur/onblur
