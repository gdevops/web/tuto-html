.. index::
   pair: HTMLElement ; Web API
   ! HTMLElement

.. _HTMLElement_web_api:

=========================
**HTMLElement** Web API
=========================

- https://developer.mozilla.org/fr/docs/Web/API/HTMLElement

.. toctree::
   :maxdepth: 5

   definition/definition
   input_events/input_events
