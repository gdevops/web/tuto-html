.. index::
   pair: HTMLElement ; definition

.. _HTMLElement_definition:

=========================
HTMLElement definition
=========================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/API/HTMLElement




Defintion en français
========================


L'interface **Event** représente tout événement qui a lieu dans le **DOM** ;
certains sont *générés par l'utilisateur* (tels que des événements de souris ou de clavier),
tandis que d'autres sont *générés par des API* (par exemple, des événements
indiquant qu'une animation est terminée, qu'une vidéo a été suspendue, etc.).

Il existe plusieurs types d'événements, dont certains utilisent d'autres
interfaces basées sur l'interface principale Event.

**Event** elle-même contient les propriétés et méthodes communes à tous les événements.


English definition
======================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement

**The HTMLElement interface** represents any HTML element.

Some elements directly implement this interface, while others implement it via
an interface that inherits it.
