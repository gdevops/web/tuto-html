.. index::
   pair: HTMLElement ; change event

.. _change_event:

=========================
**change event**
=========================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/change_event


.. toctree::
   :maxdepth: 3

   definition/definition
   examples/examples
