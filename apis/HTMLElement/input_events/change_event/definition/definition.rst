
.. _change_event_def:

==============================
**change event** definition
==============================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/change_event





Definition
=============

The change event is fired for <input>, <select>, and <textarea> elements when
an alteration to the element's value is committed by the user.

**Unlike the input event, the change event is not necessarily fired for each
alteration** to an element's value.
