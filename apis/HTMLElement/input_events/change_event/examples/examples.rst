
.. _change_event_examples:

===========================
**change event** examples
===========================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/change_event


.. toctree::
   :maxdepth: 3

   select/select
