
.. _change_event_select_example:

==================================
change event **select** example
==================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/change_event





HTML
=====


.. code-block:: html
   :linenos:

    <label>Choose an ice cream flavor:
      <select class="ice-cream" name="ice-cream">
        <option value="">Select One …</option>
        <option value="chocolate">Chocolate</option>
        <option value="sardine">Sardine</option>
        <option value="vanilla">Vanilla</option>
      </select>
    </label>

    <div class="result"></div>


JavaScript
===========


.. code-block:: javascript
   :linenos:

    const selectElement = document.querySelector('.ice-cream');

    selectElement.addEventListener('change', (event) => {
      const result = document.querySelector('.result');
      result.textContent = `You like ${event.target.value}`;
    });
