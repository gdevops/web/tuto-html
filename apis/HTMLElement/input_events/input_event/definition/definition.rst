
.. _input_event_def:

=========================
input event definitions
=========================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/input_event





English Definition
=======================

The input event fires when the value of an <input>, <select>, or <textarea> element
has been changed.


Définition en français
=========================

L'évènement DOM input (entrée) est déclenché de façon synchrone quand la valeur
d'un élément <input> (entrée), <select> (sélection) ou <textarea> (zone de texte)
est modifiée.

Pour les éléments input avec type=checkbox (case à cocher) ou type=radio ,
l'évènement input  n'est pas lancé quand l'utilisateur clique sur le contrôle,
parce que la valeur attribuée ne peut être changée.

De plus, l'évènement  input se déclenche sur les éditeurs contenteditable quand
son contenu est modifié.

Dans ce cas, l'évènement cible est l'élément "editing host" (hôte de l'édition).

S'il y a deux éléments ou plus qui ont contenteditable à true (vrai), "editing host"
est l'élément ancêtre le plus proche dont le parent n'est pas modifiable .

De même, il est déclenché sur l'élément racine des éditeurs designMode .

Evenement
=============

L'évènement **change** est lié .

**change** se déclenche moins souvent que "input" - il n'est lancé que lorsque
les modifications sont validées par l'utilisateur.
