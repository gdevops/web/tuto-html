
.. _input_event_example_1:

================================
input event **input** example
================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/input_event

.. contents::
   :depth: 2


HTML
======

.. code-block:: html
   :linenos:

    <input placeholder="Enter some text" name="name"/>
    <p id="values"></p>


JavaScript
==========

.. code-block:: javascript
   :linenos:

    const input = document.querySelector('input');
    const log = document.getElementById('values');

    input.addEventListener('input', updateValue);

    function updateValue(e) {
      log.textContent = e.target.value;
    }
