.. index::
   pair: HTMLElement ; input event

.. _input_event:

=========================
**input event**
=========================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/input_event


.. toctree::
   :maxdepth: 3

   definition/definition
   examples/examples
