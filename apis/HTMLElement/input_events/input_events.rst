.. index::
   pair: HTMLElement ; input events

.. _input_events:

=========================
input events
=========================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/API/HTMLElement

.. toctree::
   :maxdepth: 3


   change_event/change_event
   input_event/input_event
