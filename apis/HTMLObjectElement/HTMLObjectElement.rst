.. index::
   pair: HTMLObjectElement ; Web API
   ! HTMLObjectElement

.. _HTMLObjectElement_web_api:

================================
**HTMLObjectElement** Web API
================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/HTMLObjectElement

.. toctree::
   :maxdepth: 5


   properties/properties
