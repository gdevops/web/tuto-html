

.. _HTMLObjectElement_contentDocument:

=======================================
HTMLObjectElement.contentDocument
=======================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/HTMLObjectElement




Definition
=============

The contentDocument read-only property of the HTMLObjectElement interface
returns a Document representing the active document of the object element's
nested browsing context, if any; otherwise null.


Examples
===========

::

    let designation = document.getElementById('id_codifnomenclature_related-0-designation_iframe').contentDocument.activeElement.innerHTML;
