

.. _HTMLObjectElement_properties:

====================================
**HTMLObjectElement** properties
====================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/HTMLObjectElement

.. toctree::
   :maxdepth: 5


   contentDocument/contentDocument
