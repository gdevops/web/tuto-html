
.. _HTMLTableElement_def:

==================================
**HTMLTableElement** definition
==================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/HTMLTableElement




Definition
===========

The HTMLTableElement interface provides special properties and methods
(beyond the regular HTMLElement object interface it also has available
to it by inheritance) for manipulating the layout and presentation of
tables in an HTML document.


Interface de l'élément HTML Table
====================================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/API/HTMLTableElement

Les objets table exposent l'interface HTMLTableElement (traduction),
qui fournit des propriétés et méthodes spécialisées (outre l'interface
des objets element qu'ils acquièrent également par héritage) pour
manipuler la disposition et la présentation des tableaux en HTML.
