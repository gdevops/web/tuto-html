
.. _HTMLTableRowElement_def:

==================================
**HTMLTableRowElement** definition
==================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/HTMLTableRowElement
   - :ref:`table_element_def`



Definition
===============

The HTMLTableRowElement interface provides special properties and methods
(beyond the HTMLElement interface it also has available to it by inheritance)
for manipulating the layout and presentation of rows in an HTML table.
