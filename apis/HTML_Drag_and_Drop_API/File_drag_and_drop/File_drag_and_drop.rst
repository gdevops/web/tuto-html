.. index::
   pair: File; drag_and_drop
   ! File_drag_and_drop

.. _File_drag_and_drop:

=========================
File_drag_and_drop
=========================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/HTML_Drag_and_Drop_API/File_drag_and_drop
   - https://github.com/mdn/dom-examples/tree/master/drag-and-drop




Definition
=============

HTML Drag and Drop interfaces enable web applications to drag and drop
files on a web page.

This document describes how an application can accept one or more files
that are dragged from the underlying platform's file manager and dropped
on a web page.

The main steps to drag and drop are to define a drop zone (i.e. a target
element for the file drop) and to define event handlers for the drop
and dragover events.

These steps are described below, including example code snippets.

The `full source code`_ is available in MDN's drag-and-drop repository
(pull requests and/or issues are welcome).

.. _`full source code`:  https://github.com/mdn/dom-examples/tree/master/drag-and-drop
