.. index::
   pair: HTML ; Drag_and_Drop_API
   ! HTML_Drag_and_Drop_API

.. _HTML_Drag_and_Drop_API:

=========================
HTML_Drag_and_Drop_API
=========================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/HTML_Drag_and_Drop_API
   - https://www.w3schools.com/HTML/html5_draganddrop.asp

.. toctree::
   :maxdepth: 3

   definition/definition
   examples/examples
   File_drag_and_drop/File_drag_and_drop
