
.. _HTML_Drag_and_Drop_API_def:

======================================
HTML_Drag_and_Drop_API **definition**
======================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/HTML_Drag_and_Drop_API
   - https://www.w3schools.com/HTML/html5_draganddrop.asp




Definition
============

HTML Drag and Drop interfaces enable applications to use drag-and-drop
features in browsers.
The user may select draggable elements with a mouse, drag those elements
to a droppable element, and drop them by releasing the mouse button.

A translucent representation of the draggable elements follows the pointer
during the drag operation.

For web sites, extensions, and XUL applications, you can customize which
elements can become draggable, the type of feedback the draggable elements
produce, and the droppable elements.

This overview of HTML Drag and Drop includes a description of the
interfaces, basic steps to add drag-and-drop support to an application,
and an interoperability summary of the interfaces.


Définition en français
==========================

L'interface HTML Drag and Drop (pour glisser-déposer) permet à des
applications d'utiliser des fonctionnalités de glisser-déposer dans
le navigateur.

L'utilisateur pourra sélectionner des éléments déplaçables à la souris
et les déplacer vers un élément où on peut déposer en relâchant le bouton
de la souris.

Une représentation translucide de l'élément déplacé suit le pointeur
lors de l'opération.

Pour les sites web et les extensions, on peut personnaliser les éléments
qui peuvent être déplacés, la façon dont ceux-ci sont signalés et les
éléments qui peuvent servir de destination.

L'aperçu de cette API inclut une description des interfaces, les étapes
à suivre pour prendre en charge ces fonctionnalités dans une application
et un aperçu de l'interopérabilité de ces interfaces.

Drag Events
=============

HTML drag-and-drop uses the DOM event model and drag events inherited
from mouse events.

A typical drag operation begins when a user selects a draggable element,
drags the element to a droppable element, and then releases the dragged
element.

During drag operations, several event types are fired, and some events
might fire many times, such as the drag and dragover events.

Each drag event type has an associated global event handler:
