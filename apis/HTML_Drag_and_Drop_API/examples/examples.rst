

.. _HTML_Drag_and_Drop_API_examples:

=====================================
HTML_Drag_and_Drop_API **examples**
=====================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/HTML_Drag_and_Drop_API
   - https://www.w3schools.com/HTML/html5_draganddrop.asp

.. toctree::
   :maxdepth: 3

   w3schools/w3schools
