

.. _HTML_Drag_and_Drop_API_w3schools:

=====================================
HTML_Drag_and_Drop_API **w3schools**
=====================================

.. seealso::

   - https://www.w3schools.com/HTML/html5_draganddrop.asp
   - https://www.w3schools.com/howto/howto_js_draggable.asp
