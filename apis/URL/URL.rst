.. index::
   pair: URL ; Web API
   ! URL

.. _URL:

=============================
**URL** Web API
=============================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/URL



Description
=============

The URL interface is used to parse, construct, normalize, and encode URLs.

It works by providing properties which allow you to easily read and
modify the components of a URL.

You normally create a new URL object by specifying the URL as a string
when calling its constructor, or by providing a relative URL and a base URL.

You can then easily read the parsed components of the URL or make changes
to the URL.

If a browser doesn't yet support the URL() constructor, you can access
a URL object using the Window interface's Window.URL property.

Be sure to check to see if any of your target browsers require this
to be prefixed.

Examples
=========

To get the search params from the current window's URL, you can do this:

.. code-block:: javascript

    // https://some.site/?id=123
    const parsedUrl = new URL(window.location.href);
    console.log(parsedUrl.searchParams.get("id")); // "123"
