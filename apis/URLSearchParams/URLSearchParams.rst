.. index::
   pair: URLSearchParams ; Web API
   ! URLSearchParams

.. _URLSearchParams:

=============================
**URLSearchParams** Web API
=============================

.. seealso::

   - https://url.spec.whatwg.org/#urlsearchparams
   - https://developer.mozilla.org/en-US/docs/Web/API/URLSearchParams



Description
=============

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/URLSearchParams


The URLSearchParams interface defines utility methods to work with the
query string of a URL.

An object implementing URLSearchParams can directly be used in a
for...of structure, for example the following two lines are equivalent:

.. code-block:: javascript

    for (const [key, value] of mySearchParams) {}
    for (const [key, value] of mySearchParams.entries()) {}



Tutos
=========


.. toctree::
   :maxdepth: 3

   tutos/tutos
