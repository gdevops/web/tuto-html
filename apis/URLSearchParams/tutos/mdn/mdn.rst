

.. _URLSearchParams_mdn:

=============================
Mozilla tutorial
=============================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/URLSearchParams/set



Let's start with a simple example
=======================================

.. code-block:: javascript

    let url = new URL('https://example.com?foo=1&bar=2');
    let params = new URLSearchParams(url.search.slice(1));

    //Add a third parameter.
    params.set('baz', 3);
    params.toString(); // "foo=1&bar=2&baz=3"


A real-life example demonstrating how to create a URL and set some search parameters
=======================================================================================

Below is a real-life example demonstrating how to create a URL and set
some search parameters.

You can copy and paste the example in a code environment like Codepen,
JSFiddle, or the multi-line JavaScript interpreter in Firefox.

- line #41: Comment out this line to stop dumping the search parameters to the console (debug).
- line #43: Dumps the generated object and it's string representation to the console (info).
- line #44: Tries to automatically open a new window/tab with the generated URL (when uncommented).

.. code-block:: javascript

    'use strict'

    function genURL(rExp, aText, bDebug=false){
        let theURL

        theURL= new URL('https://regexr.com')
        theURL.searchParams.set( 'expression', rExp.toString() )
        theURL.searchParams.set( 'tool', 'replace' )
        theURL.searchParams.set( 'input', '\u2911\u20dc' )// ⤑⃜
        theURL.searchParams.set( 'text', aText.join('\n') )
        if( bDebug ){
            // Display the key/value pairs
            for(var pair of theURL.searchParams.entries()) {
                console.debug(pair[0] + ' = \'' + pair[1] + '\'');
            }
            console.debug(theURL)
        }
        return theURL
    }
    var url = genURL(
        /(^\s*\/\/|\s*[^:]\/\/).*\s*$|\s*\/\*(.|\n)+?\*\/\s*$/gm	// single/multi-line comments
        // /(^\s*\/\/.*|\s*[^:]\/\/.*)/g								// single-line comments
        ,[
            "These should work:",
            "",
            "// eslint-disable-next-line no-unused-vars",
            "lockPref(	'keyword.URL',\t\t'https://duckduckgo.com/html/?q=!+'	)\t//      test",
            "/*",
            "	* bla bla    ",
            "*/",
            "",
            "/* bla bla */",
            "",
            "// bla bla ",
            "",
            "These shouldn\'t work:",
            "console.log(\"http://foo.co.uk/\")",
            "var url = \"http://regexr.com/foo.html?q=bar\"",
            "alert(\"https://mediatemple.net\")",
        ]
        , true
    )
    console.info( url, url.toString() )
    // window.open( url, 'regex_site' )



Example 2
============

.. code-block:: javascript

    var paramsString = "q=URLUtils.searchParams&topic=api";
    var searchParams = new URLSearchParams(paramsString);

    //Iterate the search parameters.
    for (let p of searchParams) {
      console.log(p);
    }

    searchParams.has("topic") === true; // true
    searchParams.get("topic") === "api"; // true
    searchParams.getAll("topic"); // ["api"]
    searchParams.get("foo") === null; // true
    searchParams.append("topic", "webdev");
    searchParams.toString(); // "q=URLUtils.searchParams&topic=api&topic=webdev"
    searchParams.set("topic", "More webdev");
    searchParams.toString(); // "q=URLUtils.searchParams&topic=More+webdev"
    searchParams.delete("topic");
    searchParams.toString(); // "q=URLUtils.searchParams"
