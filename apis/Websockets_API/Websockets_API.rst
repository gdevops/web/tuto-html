.. index::
   pair: Socket ; Websockets_API
   ! Websockets_API

.. _Websockets_API:

=====================
**Websockets_API**
=====================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/Websockets_API
   - https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API/Writing_WebSocket_servers
   - https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API/Writing_a_WebSocket_server_in_Java
   - https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API/Writing_WebSocket_client_applications

.. toctree::
   :maxdepth: 3

   definition/definition
   examples/examples
