.. index::
   pair: XMLHttpRequest ; Web API
   pair: XMLHttpRequest ; AJAX
   ! XMLHttpRequest

.. _XMLHttpRequest_web_api:

===================================
**XMLHttpRequest** (XHR) Web API
===================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest
   - https://developer.mozilla.org/en-US/docs/Web/Guide/AJAX/Getting_Started

.. toctree::
   :maxdepth: 3

   description/description
   examples/examples
