.. index::
   pair: Web ; API
   pair: Web ; Interfaces
   ! Web APIs
   ! Web Interfaces

.. _web_apis:

=====================================
**Web APIs + Web Interfaces**
=====================================

- https://developer.mozilla.org/en-US/docs/Web/API
- https://developer.mozilla.org/fr/docs/Apprendre/JavaScript/Client-side_web_APIs/Introduction
- :ref:`genindex`
- :ref:`search`


.. toctree::
   :maxdepth: 4

   dataset/dataset
   data_transfer/data_transfer
   document/document
   dom/dom
   DocumentOrShadowRoot/DocumentOrShadowRoot
   element/element
   embedded_content/embedded_content
   event/event
   event_target/event_target
   fetch/fetch
   file/file
   file_list/file_list
   file_reader/file_reader
   form_data/form_data
   GlobalEventHandlers/GlobalEventHandlers
   history/history
   HTML_Drag_and_Drop_API/HTML_Drag_and_Drop_API
   HTMLElement/HTMLElement
   HTMLTableElement/HTMLTableElement
   HTMLTableRowElement/HTMLTableRowElement
   HTMLObjectElement/HTMLObjectElement
   node/node
   response/response
   URL/URL
   URLSearchParams/URLSearchParams
   Websockets_API/Websockets_API
   window/window
   XMLHttpRequest/XMLHttpRequest
