.. index::
   pair: DataTransfer ; Web API
   ! DataTransfer

.. _data_transfer:

=========================
**DataTransfer** Web API
=========================



Description
=============

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/API/DataTransfer


L'objet **DataTransfer** contient les données glissées au cours d'une opération
de glisser-déposer.

Il peut contenir un ou plusieurs éléments, du même type ou de types différents.

Pour plus d'informations sur le glisser-déposer, voir Glisser et déposer.

Cet objet est disponible depuis la propriété dataTransfer de tous les
événements de glisser. Il ne peut pas être créé séparément.


Description
=============

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/DataTransfer

The DataTransfer object is used to hold the data that is being dragged
during a drag and drop operation.
It may hold one or more data items, each of one or more data types.
For more information about drag and drop, see HTML Drag and Drop API.

This object is available from the dataTransfer property of all drag events.

Examples
============

https://github.com/suzuki11109/vanilla-dnd
---------------------------------------------

.. seealso::

   - https://github.com/suzuki11109/vanilla-dnd


.. code-block:: javascript

    dropArea.addEventListener('drop', handleDrop, false);
    function handleDrop(e) {
      let dt = e.dataTransfer;
      let files = dt.files;

      handleFiles(files);
    }


    function handleFiles(files) {
      ([...files]).forEach(uploadFile);
      ([...files]).forEach(previewFile);
    }

    function uploadFile(file) {
      console.log(`${file} ${file.name} ${file.size} ${file.type}`);
    }

    function previewFile(file) {
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = function() {
        let img = document.createElement('img');
        img.src = reader.result;
        document.getElementById('gallery').appendChild(img);
      }
    }
