.. index::
   pair: API ; dataset
   ! dataset

.. _dataset:

==========================================================
**The dataset read-only property of the HTMLElement**
==========================================================

- https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/dataset
- https://developer.mozilla.org/en-US/docs/Web/API/DOMStringMap
- https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/data-*


Description
============

The **dataset** read-only property of the HTMLElement interface provides
read/write access to `custom data attributes <https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/data-\*>`_ (https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/data-\*) (data-\*) on elements.

It exposes a **map of strings** (`DOMStringMap <https://developer.mozilla.org/en-US/docs/Web/API/DOMStringMap>`_ (https://developer.mozilla.org/en-US/docs/Web/API/DOMStringMap))
with an entry for each data-\* attribute.


.. note:: The dataset property itself can be read, but not directly written.
   Instead, all writes must be to the individual properties within the dataset,
   which in turn represent the data attributes.


An HTML data-\* attribute and its corresponding DOM **dataset.property**
modify their shared name according to where they are read or written:

In HTML
----------

The attribute name begins with data-.
It can contain only letters, numbers, dashes (-), periods (.), colons (:),
and underscores (_).
Any ASCII capital letters (A to Z) are converted to lowercase.

In JavaScript
----------------

The property name of a custom data attribute is the same as the HTML
attribute without the data- prefix, and removes single dashes (-) for
when to capitalize the property's "camelCased" name.

In addition to the information below, you'll find a how-to guide for
using HTML data attributes in `our article <https://developer.mozilla.org/en-US/docs/Learn/HTML/Howto/Use_data_attributes>`_ (https://developer.mozilla.org/en-US/docs/Learn/HTML/Howto/Use_data_attributes)


Examples
===========

.. code-block:: html

    <div id="user" data-id="1234567890" data-user="johndoe" data-date-of-birth>John Doe</div>


.. code-block:: javascript
   :linenos:

    const el = document.querySelector('#user');

    // el.id === 'user'
    // el.dataset.id === '1234567890'
    // el.dataset.user === 'johndoe'
    // el.dataset.dateOfBirth === ''

    // set a data attribute
    el.dataset.dateOfBirth = '1960-10-03';
    // Result: el.dataset.dateOfBirth === '1960-10-03'

    delete el.dataset.dateOfBirth;
    // Result: el.dataset.dateOfBirth === undefined

    if ('someDataAttr' in el.dataset === false) {
      el.dataset.someDataAttr = 'mydata';
      // Result: 'someDataAttr' in el.dataset === true
    }


Other tutorials
==================

.. toctree::
   :maxdepth: 3

   gomakethings/gomakethings
