.. index::
   pair: Document ; Web API
   ! Document

.. _html_document_def:

======================================
Document definition
======================================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/API/Document



English definition
====================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/API/Document

The Document interface represents any web page loaded in the browser and
serves as an entry point into the web page's content, which is the DOM
tree.

The DOM tree includes elements such as <body> and <table>, among many
others.

It provides functionality globally to the document, like how to obtain
the page's URL and create new elements in the document.

The Document interface describes the common properties and methods for
any kind of document. Depending on the document's type (e.g. HTML, XML, SVG, …),
a larger API is available: HTML documents, served with the "text/html"
content type, also implement the HTMLDocument interface, whereas XML
and SVG documents implement the XMLDocument interface.


Définition en français
========================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/API/Document


L'interface Document représente n'importe quelle page Web chargée dans
le navigateur et sert de point d'entrée dans le contenu de la page Web,
qui est l'arborescence DOM.

L'arborescence DOM inclut des éléments tels que <body> (corps) et <table> (tableau),
parmi beaucoup d'autres.

Il fournit des fonctionnalités globales au document, comme le moyen
d'obtenir l'URL de la page et de créer de nouveaux éléments dans le document.

L'interface Document décrit les propriétés et méthodes communes à tout
type de document.

En fonction du type de document (HTML, XML, SVG, ...), une API plus
grande est disponible : les documents HTML, servis avec le type text/html,
implémentent également l'interface HTMLDocument, alors que les documents
XML et SVG implémentent l'interface XMLDocument.
