.. index::
   pair: Document ; Web API
   ! Document

.. _html_document:

======================================
**Document API**
======================================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/API/Document

.. toctree::
   :maxdepth: 3

   definition/definition
   methods/methods
   events/events
