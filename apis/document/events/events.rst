.. index::
   pair: Document ; Events
   ! Document events

.. _document_events:

======================================
Document events
======================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/Document#Events

.. toctree::
   :maxdepth: 3

   DOMContentLoaded_event/DOMContentLoaded_event
