.. index::
   pair: createElement ; Document
   ! Document.createElement
   ! createElement

.. _create_element:

======================================
**createElement**
======================================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/API/Document/createElement
   - https://dom.spec.whatwg.org/#dom-document-createelement



Definition
===========

Dans un document HTML, la méthode document.createElement() crée un élément
HTML du type spécifié par tagName ou un HTMLUnknownElement si tagName
n’est pas reconnu.

Syntaxe
==========

::

    var element = document.createElement(tagName[, options]);

Valeur de retour
------------------

L’objet :ref:`Element <html_element>` créé.


Exemple de base
===================

Ici est créé un nouveau <div> qui est inséré avant l’élément avec
l’identifiant "div1".

HTML
------

.. code-block:: html

    <!DOCTYPE html>
    <html>
    <head>
      <title>||Working with elements||</title>
    </head>
    <body>
      <div id="div1">The text above has been created dynamically.</div>
    </body>
    </html>

JavaScript
-------------

.. code-block:: javascript

    document.body.onload = addElement;

    function addElement () {
      // crée un nouvel élément div
      var newDiv = document.createElement("div");
      // et lui donne un peu de contenu
      var newContent = document.createTextNode('Hi there and greetings!');
      // ajoute le nœud texte au nouveau div créé
      newDiv.appendChild(newContent);

      // ajoute le nouvel élément créé et son contenu dans le DOM
      var currentDiv = document.getElementById('div1');
      document.body.insertBefore(newDiv, currentDiv);
    }

Exemple - https://github.com/suzuki11109/vanilla-dnd
=========================================================

.. seealso::

   - - https://github.com/suzuki11109/vanilla-dnd

.. code-block:: javascript

    function previewFile(file) {

      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = function() {
        let img = document.createElement('img');
        img.src = reader.result;
        document.getElementById('gallery').appendChild(img);
      }
    }


Exemple de composant web
============================

L’exemple de fragment suivant est extrait de notre exemple expanding-list-web-component
(voir aussi en direct).

Dans ce cas, notre élément personnalisé étend la HTMLUListElement qui
représente l’élément <ul>.

.. code-block:: javascript

    // Crée une classe pour l’élément
    class ExpandingList extends HTMLUListElement {
      constructor() {
        // Toujours appeler « super » en premier dans le constructeur
        super();

        // définition du constructeur omise pour la brièveté
        ...
      }
    }

    // Définit le nouvel élément
    customElements.define('expanding-list', ExpandingList, { extends: 'ul' });

Si nous cherchons à créer une instance de cet élément par programmation,
nous devons utiliser un appel tel que montré dans la ligne suivante::

    let expandingList = document.createElement('ul', { is : 'expanding-list' })

Le nouvel élément donnera un attribut is dont la valeur est la balise
de nom de l’élément personnalisé.
