.. index::
   pair: getElementById ; Methods

.. _getElementById:

======================================
**getElementById**
======================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/Document/getElementById
   - :ref:`id_element`



Definition
===========

The Document method **getElementById()** returns an Element object representing
the element whose id property matches the specified string.

**Since element IDs are required to be unique** if specified, they're a
useful way to get access to a specific element quickly.

If you need to get access to an element which doesn't have an ID, you
can use querySelector() to find the element using any selector.

Définition en français
=========================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/API/Document/getElementById

La méthode **getElementById()** de Document renvoie un objet  Element
représentant l'élément dont la propriété :ref:`id <id_element>` correspond à la chaîne
de caractères spécifiée.

Étant donné que les ID d'élément **doivent être uniques**, s'ils sont
spécifiés, ils constituent un moyen utile d'accéder rapidement à
un élément spécifique.

Si vous avez besoin d'accéder à un élément qui n'a pas d'ID, vous pouvez
utiliser querySelector() pour trouver l'élément en utilisant un sélecteur.


Syntaxe
=========

::

    var element = document.getElementById(id);

id
---

L'ID (identifiant) de l'élément à localiser.

Il est une chaîne de caractères sensible à la casse qui est **unique** ;
un seul élément peut avoir un ID donné.


Example simple
=================

::

    let current_iso_week = document.getElementById("id_current_iso_week");

Exemple - https://github.com/suzuki11109/vanilla-dnd
=========================================================

.. seealso::

   - - https://github.com/suzuki11109/vanilla-dnd

.. code-block:: javascript


    function previewFile(file) {
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = function() {
        let img = document.createElement('img');
        img.src = reader.result;
        document.getElementById('gallery').appendChild(img);
      }
    }
