.. index::
   pair: getElementsByClassName ; Method

.. _getElementsByClassName:

======================================
**getElementsByClassName**
======================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/Document/getElementsByClassName



Definition
===========

The **getElementsByClassName** method of Document interface returns an
array-like object of all child elements which have all of the given
class name(s).

When called on the document object, the complete document is searched,
including the root node.

You may also call getElementsByClassName() on any element; it will return
only elements which are descendants of the specified root element with
the given class name(s).


Définition en français
=========================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/API/Document/getElementsByClassName

Renvoie un objet de type tableau de tous les éléments enfants qui ont
tous les noms de classe donnés.

Lorsqu'il est appelé sur l'objet document, le document complet est recherché,
y compris le nœud racine. Vous pouvez également appeler getElementsByClassName ()
sur n'importe quel élément; il retournera uniquement les éléments qui
sont les descendants de l'élément racine spécifié avec les noms de classes donnés.


Examples
==========

Example 1 with django-summernote
----------------------------------

.. seealso::

   - https://github.com/summernote/summernote
   - https://github.com/summernote/awesome-summernote
   - https://github.com/summernote/django-summernote
   - https://github.com/summernote


::

    function trt_iframes_responses()
    {
        console.log(`trt_iframes_reponses() document.readyState = ${document.readyState}`);

        {% for form_achat in formset_achats %}
            try
            {
                {# le champ reponse n'est pas toujours present #}
                let iframe_reponse_{{ forloop.counter0 }} = document.getElementById('id_achatservice_related-{{ forloop.counter0 }}-reponse_iframe');
                iframe_reponse_{{ forloop.counter0 }}.contentDocument.activeElement.onblur = function() {
                    let editables = iframe_reponse_{{ forloop.counter0 }}.contentDocument.getElementsByClassName('note-editable');
                    if (editables.length > 0) {
                        let editable = editables[0];
                        let reponse = editable.innerHTML;
                        let achatservice_id = $('#id_achatservice_related-{{ forloop.counter0 }}-id').val();
                        let url = '{% url "articles:ajax_update_achat_service" %}';
                        $.ajax(
                                {
                                    url: url,
                                    data: {
                                        'id': achatservice_id,
                                        'reponse': reponse,
                                    },
                                    dataType: 'json',
                                    success: function (data) {
                                        globalThis.increment_update();
                                        console.log(`data.update=${data.update}`);
                                    }
                            }
                        );
                    };
                }
            } catch (error) {
                ; // console.error(error);
            }

        {% endfor %}
    }



    window.addEventListener('load', (event) => {
        console.log('window page is fully loaded');

        {% comment %}
        On ne traite les iframes que lorsque tout est chargé
        voir:
        - https://javascript.info/onload-ondomcontentloaded#readystate
        - https://developer.mozilla.org/en-US/docs/Web/API/Window/load_event
        {% endcomment %}

        trt_iframe_description_demande();
        trt_iframes_responses();
        trt_iframes_descriptions();
    });
