.. index::
   pair: Document ; Methods

.. _html_document_methods:

======================================
Document **methods**
======================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/Document

.. toctree::
   :maxdepth: 3

   create_element/create_element
   getElementById/getElementById
   getElementsByClassName/getElementsByClassName
   querySelectorAll/querySelectorAll
