.. index::
   pair: querySelectorAll ; Method

.. _querySelectorAll:

======================================
**querySelectorAll**
======================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/Document/querySelectorAll



Definition
===========

The Document method querySelectorAll() returns a static (not live)
NodeList representing a list of the document's elements that match
the specified group of selectors.

Définition en français
=========================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/API/Document/querySelectorAll

La méthode querySelectorAll() de Element renvoie une NodeList statique
représentant une liste des éléments du document qui correspondent au
groupe de sélecteurs spécifiés.



Syntaxe
----------

::

    elements = document.querySelectorAll(selecteurs);

Paramètres
+++++++++++++++

selecteurs
    une DOMString (chaîne de caractères) qui contient un ou plusieurs
    sélecteurs CSS ; s'il n'y en a pas, une exception  SyntaxError est
    lancée. Voir localisation des éléments DOM avec les sélecteurs pour
    plus d'informations sur l'utilisation des sélecteurs en vue
    d'identifier les éléments.

    Plusieurs sélecteurs peuvent être spécifiés, séparés par une virgule.
