.. index::
   pair: DOM ; Web API
   ! DOM

.. _dom:

======================================
**Document Object Model** (DOM)
======================================

.. seealso::

   - https://dom.spec.whatwg.org/

.. toctree::
   :maxdepth: 3

   mozilla/mozilla
