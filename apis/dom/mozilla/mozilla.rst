.. index::
   pair: DOM ; Mozilla

.. _dom_mozilla:

================================================
**Document Object Model** (DOM)  mozilla MDN
================================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model
   - https://developer.mozilla.org/en-US/docs/Glossary/DOM



Definition
============

The Document Object Model (DOM) connects web pages to scripts or
programming languages by representing the structure of a document—such
as the HTML representing a web page—in memory.

Usually **that means JavaScript**, although modeling HTML, SVG, or XML
documents as objects is not part of the core JavaScript language, as such.

**The DOM represents a document with a logical tree**.

Each branch of the tree ends in a node, and each node contains objects.

DOM methods allow programmatic access to the tree. With them you can
change the document's structure, style, or content.

Nodes can also have **event handlers** attached to them.

Once an event is **triggered**, the event handlers get executed.


Definition en français (glossaire)
===================================

Le DOM (Document Object Model) est une API qui réprésente et interagit
avec tous types de documents HTML ou XML.

Le DOM est un modèle de document chargé dans le navigateur.

La représentation du document est un arbre nodal.

Chaque nœud représente une partie du document (par exemple, un élément,
une chaîne de caractères ou un commentaire).

Le DOM est l'une des API les plus utilisées sur le Web parce-qu'il
autorise du code exécuté dans un navigateur à accéder et interagir
avec chaque nœud dans le document.

Les nœuds peuvent être créés, déplacés et modifiés.

Des auditeurs d'évènements (event listeners) peuvent être ajoutés à des
nœuds et déclenchés par un évènement donné.

À l'origine, DOM n'était pas standardisé.

Il ne l'a été que lorsque les navigateurs ont commencé à implémenter
JavaScript.

Le DOM qui découle de cette période initiale est parfois appelé DOM 0.

À l'heure actuelle, le W3C édicte les spécifications de la norme DOM
