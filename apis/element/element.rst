.. index::
   pair: Element ; Web API
   ! Element

.. _Element_web_api:

=========================
**Element** Web API
=========================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/Element

.. toctree::
   :maxdepth: 5


   events/events
   properties/properties
   table/table
