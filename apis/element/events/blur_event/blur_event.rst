.. index::
   pair: Element ; blur event
   ! blur event


.. _element_blur_event:

====================================
Element: **blur event** (onblur)
====================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/Element/blur_event
   - :ref:`GlobalEventHandlers_onblur`




Description
============

**The blur event fires when an element has lost focus**.

The main difference between this event and focusout is that focusout
bubbles while blur does not.


Interface: **FocusEvent**
===========================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/FocusEvent


Event handler property: **onblur**
======================================

.. seealso::

   - :ref:`GlobalEventHandlers_web_api`



Examples
=========


.. code-block:: django
   :linenos:

    {% for form_codif_nomenclature in formset_codif_nomenclature %}
        var designation_mde_{{ forloop.counter0 }} = new SimpleMDE(
            {
                element: document.getElementById('id_codifnomenclature_related-{{ forloop.counter0 }}-designation'),
                toolbar: false,
                status: false,
                spellChecker: false,
            });

        {# Par défaut le mode preview est activé #}
        designation_mde_{{ forloop.counter0 }}.togglePreview();

        designation_mde_{{ forloop.counter0 }}.codemirror.on("blur", function(){
            let designation = designation_mde_{{ forloop.counter0 }}.value();
            console.log("designation", designation );
            let codifnomenclature_id = $('#id_codifnomenclature_related-{{ forloop.counter0 }}-id').val();
            let url = '{% url "articles:ajax_update_codif_nomenclature" %}';
            $.ajax(
                    {
                        url: url,
                        data: {
                            'id': codifnomenclature_id,
                            'designation': designation,
                        },
                        dataType: 'json',
                        success: function (data) {
                        }
                }
            );
        });
       document.getElementById("id_description_toggle_preview_{{ forloop.counter0 }}").onclick = function()
       {
           designation_mde_{{ forloop.counter0 }}.togglePreview();
       };

    {% endfor %}
