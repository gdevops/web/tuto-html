.. index::
   pair: Element ; click event
   ! click event
   ! onclick


.. _element_click_event:

====================================
Element: **click event** (onclick)
====================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/Element/click_event
   - :ref:`genindex`





Description
============

An element receives a click event when a pointing device button
(such as a mouse's primary mouse button) is both pressed and released
while the pointer is located inside the element.


Définition en français
=========================

L'évènement click est déclenché à partir d'un élément lorsqu'un bouton
d'un dispositif de pointage (comme celui d'une souris par exemple) est
pressé puis relaché lorsque le pointeur est sur l'élément.


Interface: **MouseEvent**
===========================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent


Event handler property: **onclick**
======================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/GlobalEventHandlers/onclick



Examples
=========


.. code-block:: django
   :linenos:

    {% for form_codif_nomenclature in formset_codif_nomenclature %}
        var designation_mde_{{ forloop.counter0 }} = new SimpleMDE(
            {
                element: document.getElementById('id_codifnomenclature_related-{{ forloop.counter0 }}-designation'),
                toolbar: false,
                status: false,
                spellChecker: false,
            });

        {# Par défaut le mode preview est activé #}
        designation_mde_{{ forloop.counter0 }}.togglePreview();

        designation_mde_{{ forloop.counter0 }}.codemirror.on("blur", function(){
            let designation = designation_mde_{{ forloop.counter0 }}.value();
            console.log("designation", designation );
            let codifnomenclature_id = $('#id_codifnomenclature_related-{{ forloop.counter0 }}-id').val();
            let url = '{% url "articles:ajax_update_codif_nomenclature" %}';
            $.ajax(
                    {
                        url: url,
                        data: {
                            'id': codifnomenclature_id,
                            'designation': designation,
                        },
                        dataType: 'json',
                        success: function (data) {
                        }
                }
            );
        });
       document.getElementById("id_description_toggle_preview_{{ forloop.counter0 }}").onclick = function()
       {
           designation_mde_{{ forloop.counter0 }}.togglePreview();
       };

    {% endfor %}
