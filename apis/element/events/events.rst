.. index::
   pair: Element ; Events

.. _Element_events:

=========================
**Element** events
=========================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/Element

.. toctree::
   :maxdepth: 5


   blur_event/blur_event
   click_event/click_event
