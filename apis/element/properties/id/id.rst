.. index::
   pair: id ; Property

.. _id_element:

=========================
**id** property
=========================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/Element/id

.. contents::
   :depth: 5

Definition
===========

The **id** property of the Element interface represents the element's
identifier, reflecting the id global attribute.

If the id value is not the empty string, it must be unique in a document.

The id is often used with :ref:`getElementById() <getElementById>` to retrieve a particular
element.

Another common case is to use an element's ID as a selector when styling
the document with CSS.


Syntax
=======

var idStr = element.id; // Get the id
element.id = idStr; // Set the id

    idStr is the identifier of the element.
