.. index::
   pair: innerHTML ; Property

.. _innerHTML:

=========================
**innerHTML** property
=========================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/Element/innerHTML



Definition
===========

The Element property **innerHTML** gets or sets the HTML or XML markup
contained within the element.

Note: If a <div>, <span>, or <noembed> node has a child text node that
includes the characters (&), (<), or (>), innerHTML returns these
characters as the HTML entities "&amp;", "&lt;" and "&gt;" respectively.

Use Node.textContent to get a raw copy of these text nodes' contents.

To insert the HTML into the document rather than replace the contents
of an element, use the method insertAdjacentHTML().


Examples
===========

::

    document.getElementById('id_description_iframe').contentDocument.activeElement.onblur = function()
    {
        // Le texte HTML issu du iframe id_description_iframe
        let description = document.getElementById('id_description_iframe').contentDocument.activeElement.innerHTML;
        console.log(`description=${description}`);
    }
