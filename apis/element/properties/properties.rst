.. index::
   pair: Element ; Properties

.. _Element_properties:

=========================
**Element** properties
=========================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/Element

.. toctree::
   :maxdepth: 5


   id/id
   innerhtml/innerhtml
