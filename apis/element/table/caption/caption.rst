.. index::
   pair: table ; caption
   pair: Element ; caption
   ! caption

.. _caption_element:

=========================
**<caption>** element
=========================

- https://developer.mozilla.org/fr/docs/Web/HTML/Element/caption


Definition
===========

The <caption> HTML element specifies the caption (or title) of a table.

Définition en français
========================

L'élément <caption> définit la légende (ou le titre) d'un tableau.

Codes
==========

Code HTML
----------

.. code-block:: html

    <table>
        <caption>He-Man and Skeletor facts</caption>
        <tr>
            <td> </td>
            <th scope="col" class="heman">He-Man</th>
            <th scope="col" class="skeletor">Skeletor</th>
        </tr>
        <tr>
            <th scope="row">Role</th>
            <td>Hero</td>
            <td>Villain</td>
        </tr>
        <tr>
            <th scope="row">Weapon</th>
            <td>Power Sword</td>
            <td>Havoc Staff</td>
        </tr>
        <tr>
            <th scope="row">Dark secret</th>
            <td>Expert florist</td>
            <td>Cries at romcoms</td>
        </tr>
    </table>



Code CSS
----------


.. code-block:: CSS

    caption {
        padding: 10px;
        caption-side: bottom;
    }

    table {
        border-collapse: collapse;
        border: 2px solid rgb(200, 200, 200);
        letter-spacing: 1px;
        font-family: sans-serif;
        font-size: .8rem;
    }

    td,
    th {
        border: 1px solid rgb(190, 190, 190);
        padding: 7px 5px;
    }

    th {
        background-color: rgb(235, 235, 235);
    }

    td {
        text-align: center;
    }

    tr:nth-child(even) td {
        background-color: rgb(250, 250, 250);
    }

    tr:nth-child(odd) td {
        background-color: rgb(240, 240, 240);
    }

    .heman {
        font: 1.4rem molot;
        text-shadow: 1px 1px 1px #fff, 2px 2px 1px #000;
    }

    .skeletor {
        font: 1.7rem rapscallion;
        letter-spacing: 3px;
        text-shadow: 1px 1px 0 #fff, 0 0 9px #000;
    }
