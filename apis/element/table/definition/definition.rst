
.. _table_element_def:

===============================
**table Element** definition
===============================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/HTML/Element/table
   - :ref:`HTMLTableElement_web_api`




Definition
===========

The HTML <table> element represents tabular data — that is, information
presented in a two-dimensional table comprised of rows and columns of
cells containing data.


Definition en français
=========================

L'élément HTML <table> permet de représenter un tableau de données,
c'est-à-dire des informations exprimées sur un tableau en deux dimensions.



Javascript
==============

.. code-block:: javascript
   :linenos:

    /**
     * Suppression d'une ligne lorsque le temps imputé === '00:00'
     *
     */
    function trt_temps_impute_0(value_temps_impute, element) {
        if (value_temps_impute === "00:00") {
            // https://gdevops.frama.io/web/linkertree/html/apis/element/element.html
            // https://gdevops.frama.io/web/linkertree/html/apis/element/table/table.html
            // https://gdevops.frama.io/web/linkertree/html/apis/HTMLTableElement/HTMLTableElement.html
            const table = document.getElementById('id_list_table');
            // https://gdevops.frama.io/web/linkertree/html/apis/node/properties/parentElement/parentElement.html
            // https://gdevops.frama.io/web/linkertree/html/apis/element/tr/tr.html
            const element_tr = element.parentElement.parentElement.parentElement;
            // https://gdevops.frama.io/web/linkertree/html/apis/HTMLTableRowElement/properties/rowIndex/rowIndex.html
            // https://gdevops.frama.io/web/linkertree/html/apis/HTMLTableElement/methods/deleteRow/deleteRow.html
            table.deleteRow(element_tr.rowIndex);
        }
    }
