
.. _style_table_element_digit:

===================================================
**How To Style a Table with CSS** by digital ocean
===================================================

- https://www.digitalocean.com/community/tutorials/how-to-style-a-table-with-css

Introduction
===============

Tables have a long and complicated history on the web. Before CSS existed,
the <table> element was the only possible avenue for creating rich design
layouts on the Web.

But creating layout with <table> was not its intended or ideal use.

Now that better layout options are available, developers can use the
<table> element for presenting tabular data as intended, much like a
spreadsheet.

This allows for semantic HTML, or using HTML elements in alignment with
their intended meaning.

