.. index::
   pair: Element ; Table
   ! Table

.. _table_element:

=========================
**table Element**
=========================

- https://developer.mozilla.org/en-US/docs/Web/HTML/Element/table
- https://www.w3.org/WAI/tutorials/tables/tips/
- https://css-tricks.com/complete-guide-table-element/

.. toctree::
   :maxdepth: 3

   definition/definition
   caption/caption
   thead/thead
   tbody/tbody
   tfoot/tfoot
   th/th
   tr/tr
   styling/styling
