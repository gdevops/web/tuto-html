.. index::
   pair: table ; tbody
   pair: Element ; tbody
   ! tbody

.. _tbody_element:

=========================
**<tbody>** element
=========================

- https://developer.mozilla.org/fr/docs/Web/HTML/Element/tbody


Definition
===========

The <tbody> HTML element encapsulates a set of table rows (<tr> elements),
indicating that they comprise the body of the table (<table>).


Définition en français
========================

L'élément HTML <tbody> permet de regrouper un ou plusieurs éléments <tr>
afin de former le corps d'un tableau HTML (<table>).


Codes
=====

Code HTML
-----------

.. code-block:: html

    <table>
        <caption>Council budget (in £) 2018</caption>
        <thead>
            <tr>
                <th>Items</th>
                <th scope="col">Expenditure</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">Donuts</th>
                <td>3,000</td>
            </tr>
            <tr>
                <th scope="row">Stationery</th>
                <td>18,000</td>
            </tr>
        </tbody>
    </table>


Code CSS
-----------

.. code-block:: css


    thead,
    tfoot {
        background-color: #3f87a6;
        color: #fff;
    }

    tbody {
        background-color: #e4f0f5;
    }

    caption {
        padding: 10px;
        caption-side: bottom;
    }

    table {
        border-collapse: collapse;
        border: 2px solid rgb(200, 200, 200);
        letter-spacing: 1px;
        font-family: sans-serif;
        font-size: .8rem;
    }

    td,
    th {
        border: 1px solid rgb(190, 190, 190);
        padding: 5px 10px;
    }

    td {
        text-align: center;
    }
