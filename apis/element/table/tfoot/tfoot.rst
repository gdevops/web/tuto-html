.. index::
   pair: table ; tfoot
   pair: Element ; tfoot
   ! tfoot

.. _tfoot_element:

=========================
**<tfoot>** element
=========================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/HTML/Element/tfoot




Definition
===========

The HTML <tfoot> element defines a set of rows summarizing the columns
of the table.


Définition en français
========================

L'élément HTML <tfoot> permet de définir un ensemble de lignes qui
résument les colonnes d'un tableau.
