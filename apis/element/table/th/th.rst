.. index::
   pair: table ; th
   pair: Element ; th
   ! th

.. _th_element:

=========================
**<th>** element
=========================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/HTML/Element/th
   - https://www.w3.org/WAI/tutorials/tables/tips/




Definition
===========

The HTML <th> element defines a cell as header of a group of table cells.

The exact nature of this group is defined by the scope and headers attributes.


Définition en français
========================

L'élément HTML <th> définit une cellule d'un tableau comme une cellule
d'en-tête pour un groupe de cellule.

La nature de ce groupe est définie grâce aux attributs scope et headers.


.. _th_accessibility:

**<th> Accessibility**
=========================

Associating <table> headers, i.e. <th> elements, with their <td> cells
enables screen readers to announce the header prior to the data.

**This considerably increases the accessibility of tables to visually
impaired users**.

There are two ways of doing it:

- Adding a scope attribute to <th> headers.
- Adding an id attribute to <th> headers and a headers attribute to every <td> element.

It is recommended to add scope attributes to <th> headers whenever possible.

Use <th id="..."> and <td headers="..."> only when <th scope="..."> is
not capable of associating cells to their headers.

This happens for very complex tables which have headers splitting the
data in multiple subtables.

See `W3C WAI Web Accessibility Tutorials <https://www.w3.org/WAI/tutorials/tables/tips/>`_ for more information.
