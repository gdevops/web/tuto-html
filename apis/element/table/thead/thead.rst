.. index::
   pair: table ; thead
   pair: Element ; thead
   ! thead

.. _thead_element:

=========================
**<thead>** element
=========================

- https://developer.mozilla.org/fr/docs/Web/HTML/Element/thead


Definition
===========

The <thead> HTML element defines a set of rows defining the head of the
columns of the table.

Définition en français
========================

L'élément <thead> définit un ensemble de lignes qui définit l'en-tête des colonnes d'un tableau.


Codes
==========

Code HTML
----------

.. code-block:: html

    <table>
        <caption>Council budget (in £) 2018</caption>
        <thead>
            <tr>
                <th scope="col">Items</th>
                <th scope="col">Expenditure</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">Donuts</th>
                <td>3,000</td>
            </tr>
            <tr>
                <th scope="row">Stationery</th>
                <td>18,000</td>
            </tr>
        </tbody>
    </table>


Code CSS
----------


.. code-block:: html

    thead,
    tfoot {
        background-color: #3f87a6;
        color: #fff;
    }

    tbody {
        background-color: #e4f0f5;
    }

    caption {
        padding: 10px;
        caption-side: bottom;
    }

    table {
        border-collapse: collapse;
        border: 2px solid rgb(200, 200, 200);
        letter-spacing: 1px;
        font-family: sans-serif;
        font-size: .8rem;
    }

    td,
    th {
        border: 1px solid rgb(190, 190, 190);
        padding: 5px 10px;
    }

    td {
        text-align: center;
    }
