.. index::
   pair: table ; tr
   pair: Element ; tr
   ! tr

.. _tr_element:

=========================
**<tr>** element
=========================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/HTML/Element/tr




Definition
===========

The HTML <tr> element defines **a row of cells** in a table.

The row's cells can then be established using a mix of <td> (data cell)
and <th> (header cell) elements.


Définition en français
========================

L'élément HTML <tr> définit une **ligne de cellules** dans un tableau.

Une ligne peut être constituée d'éléments <td> (les données des cellules)
et <th> (les cellules d'en-têtes).
