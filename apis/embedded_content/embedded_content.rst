.. index::
   ! Embedded content

.. _embedded_content:

=========================
**Embedded content**
=========================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/HTML/Element#Embedded_content

.. toctree::
   :maxdepth: 5


   iframe/iframe
