
.. _iframe_definition:

=========================
**iframe definition**
=========================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/HTML/Element/iframe

.. contents::
   :depth: 5


Definition
=============

The HTML Inline Frame element (<iframe>) represents a nested browsing context,
embedding another HTML page into the current one.

Each embedded browsing context has its own session history and document.

The browsing context that embeds the others is called the parent browsing
context.

The topmost browsing context — the one with no parent — is usually the
browser window, represented by the Window object.


Définition en français
==========================

L'élément HTML <iframe> représente un contexte de navigation imbriqué
qui permet en fait d'obtenir une page HTML intégrée dans la page courante.

Le contexte de navigation qui contient le contenu intégré est appelé
**contexte de navigation parent**.

Le contexte de navigation le plus élevé (qui n'a pas de contexte parent)
correspond généralement à la fenêtre du navigateur (cf. Window).
