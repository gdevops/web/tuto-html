.. index::
   pair: Event ; Interfaces
   ! Event interfaces

.. _event_interfaces_api:

=========================
**Event** interfaces
=========================


.. toctree::
   :maxdepth: 3

   preventDefault/preventDefault
