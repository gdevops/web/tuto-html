.. index::
   pair: Event ; preventDefault
   ! preventDefault

.. _prevent_default_interface:

====================================
**Event/preventDefault** interface
====================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/Event/preventDefault





Description
=============

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/Event/preventDefault


The Event interface's preventDefault() method tells the user agent that
if the event does not get explicitly handled, **its default action should
not be taken as it normally would be**.

The event continues to propagate as usual, unless one of its event
listeners calls stopPropagation() or stopImmediatePropagation(), either
of which terminates propagation at once.

As noted below, calling preventDefault() for a non-cancelable event,
such as one dispatched via EventTarget.dispatchEvent(), without
specifying cancelable: true has no effect.


Examples
===========

Blocking default click handling
-------------------------------------

Toggling a checkbox is the default action of clicking on a checkbox.

This example demonstrates how to prevent that from happening:

JavaScript
+++++++++++++++

.. code-block:: javascript
   :linenos:

    document.querySelector("#id-checkbox").addEventListener("click", function(event) {
             document.getElementById("output-box").innerHTML += "Sorry! <code>preventDefault()</code> won't let you check this!<br>";
             event.preventDefault();
    }, false);


Blocking carriage return + ['a'..'z']
------------------------------------------


.. code-block:: javascript
   :linenos:


    /**
     * disable_carriage_return()
     * On inactive le traitement du retour chariot ainsi que la saisie des lettres
     * de a à z.
     * Voir https://developer.mozilla.org/en-US/docs/Web/API/Element/keydown_event
     *
     */
    function disable_carriage_return() {
        console.log(`disable_carriage_return()`);
        const CHAR_CARRIAGE_RETURN_13 = 13;
        const CHAR_a_65 = 65;
        const CHAR_z_90 = 90;
        {% for tuple_indice, v in dict_fiches_temps.items %}
            document.getElementById('id_fiches_temps_{{ v }}-temps_impute').addEventListener("keydown", event => {
            console.log(`${event.code}=>${event.keyCode}`);
            if (event.keyCode === CHAR_CARRIAGE_RETURN_13) {
                // saisie du retour chariot bloquée
                event.preventDefault();
            } else if ((event.keyCode < CHAR_a_65) || (event.keyCode > CHAR_z_90)) {
                // saisie des lettres de a à z bloquée
                event.preventDefault();
            }
        });
        {% endfor %}
    };
