.. index::
   pair: EventTarget ; Web API
   ! EventTarget

.. _event_target_web_api:

=========================
**EventTarget** Web API
=========================



Description
=============

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/API/EventTarget

EventTarget est une interface DOM implémentée par des objets qui peuvent
recevoir des événements et peuvent avoir des écouteurs pour eux.

Element, Document et Window sont les cibles d'événements les plus fréquentes,
mais d'autres objets peuvent également être des cibles d'événements.

Par exemple XMLHttpRequest, AudioNode, AudioContext et autres.

De nombreuses cibles d'événements (y compris des éléments, des documents
et des fenêtres) supporte également la définition de gestionnaires
d'événements via les propriétés et attributs onevent.


Methods
========

.. toctree::
   :maxdepth: 3

   methods/methods
