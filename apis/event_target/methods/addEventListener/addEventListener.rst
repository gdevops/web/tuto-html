.. index::
   pair: EventTarget ; addEventListener
   ! addEventListener

.. _add_event_listener:

=====================================
**EventTarget.addEventListener()**
=====================================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/API/EventTarget/addEventListener




Description
============

La méthode addEventListener() d'EventTarget installe une fonction à
appeler chaque fois que l'événement spécifié est envoyé à la cible.

Les cibles courantes sont un Element, le Document lui-même et une Window,
mais elle peut être tout objet prenant en charge les évènements (comme XMLHttpRequest).

addEventListener() fonctionne en ajoutant une fonction, ou un objet
implémentant EventListener, à la liste des écouteurs d'évènements du
type d'évènement spécifié dans la EventTarget dans laquelle il est appelé.

Syntaxe
==========
::

    target.addEventListener(type, écouteur [, options]);
    target.addEventListener(type, écouteur [, utiliserCapture]);
    target.addEventListener(type, écouteur [, utiliserCapture, veutNonFiables  ]); // Gecko/Mozilla seulement


Le rappel de l'écouteur d'évènement
====================================

L'écouteur d'évènement peut être spécifié, soit comme une fonction de
rappel, soit comme un objet qui implémente EventListener, dont la méthode
handleEvent () sert de fonction de rappel.

La fonction de rappel a elle-même les mêmes paramètres et la même valeur
de retour que la méthode handleEvent() ; c'est-à-dire que le rappel
accepte un seul paramètre : un objet basé sur Event décrivant
l'événement qui s'est produit, et il ne retourne rien.

Par exemple, un rappel de gestionnaire d'événements pouvant être utilisé
pour gérer à la fois fullscreenchange et fullscreenerror peut ressembler
à ceci.

.. code-block:: javascript
   :linenos:

    function gestionnaireDEvenement(evenement) {
      if (evenement.type == fullscreenchange) {
        /* gérer un passage en plein écran */
      } else /* fullscreenerror */ {
        /* gérer une erreur de passage en plein écran */
      }
    }


Écouteur d'évènement avec une fonction anonyme
===================================================

Ici, nous allons voir comment utiliser une fonction anonyme pour passer
des paramètres à l'écouteur d'événements.

HTML
-----

.. code-block:: html

    <table id="aLExterieur">
        <tr><td id="t1">un</td></tr>
        <tr><td id="t2">deux</td></tr>
    </table>

JavaScript
--------------

.. code-block:: javascript

    // Fonction pour changer le contenu de t2
    function modifierTexte(nouveau_texte) {
      var t2 = document.getElementById("t2");
      t2.firstChild.nodeValue = nouveau_texte;
    }

    // Fonction pour ajouter un écouteur d'évènement à la table
    var el = document.getElementById("aLExterieur");
    el.addEventListener("click", function(){modifierTexte("quatre")}, false);

Notez que l'écouteur est une fonction anonyme encapsulant le code qui
peut à son tour envoyer des paramètres à la fonction modifierTexte(),
qui est responsable de la réponse effective à l'événement.

La valeur de "this" à l'intérieur du gestionnaire
====================================================

Il est souvent souhaitable de référencer l'élément sur lequel le
gestionnaire d'évènements a été déclenché, comme lors de l'utilisation
d'un gestionnaire générique pour un ensemble d'éléments similaires.

Lorsqu'une fonction gestionnaire est attachée à un élément en utilisant
addEventListener(), la valeur de this à l'intérieur du gestionnaire est
une référence à l'élément.

C'est la même valeur que celle de la propriété currentTarget de
l'argument évènement qui est passé au gestionnaire.

.. code-block:: javascript

    mon_element.addEventListener('click', function (e) {
      console.log(this.className)           // journalise le className de mon_element
      console.log(e.currentTarget === this) // journalise `true`
    })

Pour mémoire, les fonctions fléchées n'ont pas leur propre contexte this.

::

    mon_element.addEventListener('click', (e) => {
      console.log(this.className)           // ATTENTION : `this` n'est pas `mon_element`
      console.log(e.currentTarget === this) // journalise `false`
    })
