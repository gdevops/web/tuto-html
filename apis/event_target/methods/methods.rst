.. index::
   pair: EventTarget ; Methods

.. _event_target_methods:

=========================
**EventTarget** methods
=========================

.. toctree::
   :maxdepth: 3

   addEventListener/addEventListener
