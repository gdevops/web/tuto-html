.. index::
   pair: Fetch ; Web API
   ! Fetch

.. _fetch_web_api:

=====================
**Fetch** Web API
=====================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API
   - https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
   - https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/fetch
   - https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Client-side_web_APIs/Fetching_data

.. toctree::
   :maxdepth: 3

   definition/definition
   examples/examples
