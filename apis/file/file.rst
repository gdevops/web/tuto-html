.. index::
   pair: File ; Web API
   ! File

.. _file_web_api:

=========================
**File** Web API
=========================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/API/File



Description
=============


L’interface File fournit des informations sur des fichiers et permet au
code JavaScript d’une une page web d’accéder à leurs contenus.

Les objets File sont généralements obtenus à partir de :

- l’objet FileList retourné lorsque qu’un utilisateur ou une utilisatrice
  sélectionne des fichiers grâce à un élément <input> ;
- l’objet DataTransfer d’une opération de glisser-déposer ;
- l’API mozGetAsFile() de l’élément HTMLCanvasElement.
