.. index::
   pair: FileList ; Web API
   ! FileList

.. _filelist_web_api:

=========================
**FileList** Web API
=========================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/API/FileList



Description
=============

Un objet **FileList** est renvoyé par la propriété files d'un élément
HTML <input>.

Il permet d'accéder à la liste des fichiers sélectionnés via l'élément
<input type="file">.

Cet objet peut également être utilisé pour les fichiers glissés-déposés
dans du contenu web via l'API Drag & Drop (voir l'objet :ref:`DataTransfer <data_transfer>`
pour plus de détails).


Utiliser une liste de fichiers
==================================

Tous les éléments <input> possèdent **un attribut files de type FileList**
qui permet d'accéder aux éléments de cette liste.

Ainsi, si le code HTML utilisé est::

    <input id="fileItem" type="file">

On pourra utiliser la ligne suivant pour récupérer le premier fichier de
la liste sous la forme d'un objet File::

    var file = document.getElementById('fileItem').files[0]

Exemples
=========


https://github.com/suzuki11109/vanilla-dnd
--------------------------------------------

.. code-block:: javascript

    dropArea.addEventListener('drop', handleDrop, false);
    function handleDrop(e) {
      let dt = e.dataTransfer;
      let files = dt.files;

      handleFiles(files);
    }


    function handleFiles(files) {
      ([...files]).forEach(uploadFile);
      ([...files]).forEach(previewFile);
    }

    function uploadFile(file) {
      console.log(`${file} ${file.name} ${file.size} ${file.type}`);
    }

    function previewFile(file) {
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = function() {
        let img = document.createElement('img');
        img.src = reader.result;
        document.getElementById('gallery').appendChild(img);
      }
    }
