.. index::
   pair: FileReader ; Web API
   ! FileReader

.. _file_reader:

=========================
**FileReader** Web API
=========================



Description
=============

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/API/FileReader

L'objet FileReader permet à des applications web de lire le contenu de
fichiers (ou de tampons de mémoire brute) de façon asynchrone.

On peut ainsi lire le contenu des objets File ou Blob (qui représentent
respectivement un fichier ou des données).

Les objets qui sont des fichiers peuvent être obtenus à partir d'un objet
FileList, renvoyé lorsque l'utilisateur sélectionne des fichiers grâce à
un élément <input>, via un glisser-déposer avec un objet DataTransfer
ou grâce à l'API mozGetAsFile() API de HTMLCanvasElement.


**FileReader.readAsDataURL()**
=================================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/API/FileReader/readAsDataURL

La méthode readAsDataURL permet de lire le contenu de l’objet  Blob ou
File spécifié.

À la fin de l’opération de lecture, la propriété readyState renvoie
l’état DONE, et l’évènement **loadend** se déclenche.

À ce moment-là, l’attribut result contient les données dans une URL
représentant les données du fichier sous forme de chaîne encodée en base64.


Example
-----------

.. seealso::

   - https://github.com/suzuki11109/vanilla-dnd/blob/master/index.js

.. code-block:: javascript

    function previewFile(file) {
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = function() {
        let img = document.createElement('img');
        img.src = reader.result;
        document.getElementById('gallery').appendChild(img);
      }
    }
