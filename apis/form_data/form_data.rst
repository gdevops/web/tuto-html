.. index::
   pair: FormData ; Web API
   pair: FormData ; Web Interface
   ! FormData

.. _form_data:

============================
**FormData** Web Interface
============================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/FormData




English Description
===================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/FormData


The FormData interface provides a way to easily construct a set of
key/value pairs representing form fields and their values, which can
then be easily sent using the XMLHttpRequest.send() method.

**It uses the same format a form would use** if the encoding type were
set to "multipart/form-data".

You can also pass it directly to the :ref:`URLSearchParams constructor <URLSearchParams>`
if you want to generate query parameters in the way a <form> would do if
it were using simple GET submission.

An object implementing FormData can directly be used in a for...of structure,
instead of entries():

.. code-block:: javascript

    for (var p of myFormData)

is equivalent to:

.. code-block:: javascript

    for (var p of myFormData.entries())


Description en Français
========================

L'interface FormData permet de construire facilement un ensemble de
paires clé/valeur représentant les **champs du formulaire** et leurs valeurs,
qui peuvent ensuite être **facilement envoyées** en utilisant la
méthode XMLHttpRequest.send() de l'objet XMLHttpRequest.

Il utilise le même format qu'utilise un formulaire si le type d'encodage
est mis à "multipart/form-data".

Vous pouvez également le passer directement au constructeur URLSearchParams
si vous souhaitez générer des paramètres de requête de la même manière
qu'un <form> le ferait s'il utilisait une simple soumission GET.

Un objet implémentant FormData peut être utilisé directement dans
une structure for...of, au lieu de entries() : for (var p of myFormData)
est équivalent à for (var p of myFormData.entries()).


Tutos
======

.. toctree::
   :maxdepth: 3

   tutos/tutos
