.. index::
   pair: crudlfap ; FormData


.. _form_data_crudlfap:

===========================================================
**crudlfap** by **yourlab.io and jpic**
===========================================================

.. seealso::

   - https://yourlabs.io/oss/crudlfap/-/blob/master/js/controllers/form_controller.js
   - https://yourlabs.io/oss/crudlfap
   - https://yourlabs.io/explore/projects/
   - https://yourlabs.io/oss/ryzom
   - https://yourlabs.io/oss/crudlfap
   - https://github.com/TriOptima/iommi
   - https://github.com/jpic
   - https://yourlabs.io/jpic
   - https://github.com/yourlabs/djwc
   - https://yourlabs.org/posts/2020-07-27-the-3-problems-of-django/
   - https://github.com/yourlabs/django-autocomplete-light/issues/1184#issuecomment-710084940




Description
============


**All CRUDLFA+ forms** are sent with :ref:`FormData <form_data>` in
a `StimulusJS controller <https://yourlabs.io/oss/crudlfap/-/blob/master/js/controllers/form_controller.js>`_
(I thought I used to have file upload code in there ?!) **that we want
to convert to Web Component** like we already have from https://gitub.com/yourlabs/jquery-autocomplete-light
to https://github.com/yourlabs/autocomplete-light

Then I think we'll have gone back the full circle from when Angular
proposed to augment HTML at the time data-* attributes were new and
cool ...
but this time the browser has the core of modern JS component
frameworks standard and built-in !

Many Python developers already generate HTML components in a modern
way I mean without templates, it's the case for Iommi, Ryzom,
Django-Material v2 branch, CRUDLFA+ ryzom branch ...

Exciting times really !

Keep up the great work
