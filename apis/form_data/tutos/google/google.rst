
.. _formdata_google:

======================================================================
**FormData and fetch()** by Google
======================================================================

.. seealso::

   - https://developers.google.com/web/updates/2016/01/urlsearchparams




Integration with other places URLs are used
==============================================

By default, **sending FormData in a fetch() API request creates a multipart
body**.

If you need it, URLSearchParams provides an alternative mechanism to
POST data that's urlencoded rather than mime multipart.


.. code-block:: javascript

    const params = new URLSearchParams();
    params.append('api_key', '1234567890');

    fetch('https://example.com/api', {
      method: 'POST',
      body: params
    }).then(...)

URLSearchParams also integrates with the URL constructor and a tags.

Both support our new buddy by providing a read-only property, .searchParams
for accessing query params:

.. code-block:: javascript

    const url = new URL(location);
    const foo = url.searchParams.get('foo') || 'somedefault';

Links also get a .searchParams property:

.. code-block:: javascript

    const a = document.createElement('a');
    a.href = 'https://example.com?filter=api';

    // a.searchParams.get('filter') === 'api';
