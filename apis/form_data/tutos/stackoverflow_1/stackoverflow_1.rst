
.. _form_data_stackover_1:

=================================================================================
How to send the **formdata from the ajax to Django** view in PUT request?
=================================================================================

.. seealso::

   - https://stackoverflow.com/questions/59874632/how-to-send-the-formdata-from-the-ajax-to-django-view-in-put-request





Problem
========

I am sending the ajax call to the python django view(no rest framework)
and want to perform the update operation in django using PUT request.

This update operation involved files too so I don't know to do PUT
operation for files.

I have performed GET, POST, DELETE operations but not understanding the PUT.

I am sending the formdata object from the ajax. Thanks in advance!!


Code
=====

.. code-block:: html

    <!DOCTYPE html>
    <html lang="en">
    <head>
      <title>Bootstrap Example</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    </head>
    <body>

    <div class="container">
      <form action="" id="submitassignment" method="post" role="form" enctype="multipart/form-data">
        <div class="form-group">
            <input type="hidden" value="{{ obj.id }}" id="id">
          <label for="email">Name:</label>
          <input type="text" class="form-control" id="name" placeholder="Enter email" name="name" value="{{ obj.name }}">
        </div>
        <div class="form-group">
          <label for="pwd">age:</label>
          <input type="number" class="form-control" id="age" placeholder="Enter password" name="age" value="{{ obj.age }}">
        </div>
          <div class="form-group">
          <label for="pwd">profile_photo:</label>
          <input type="file" class="form-control" id="profile_photo" placeholder="Enter password" name="profile_photo" value="{{ obj.profile_photo.url }}">
        </div>
        <div class="form-group form-check">
          <label class="form-check-label">
            <input class="form-check-input" type="checkbox" name="remember"> Remember me
          </label>
        </div>
        <button class="btn btn-success btn-sm" type="submit" form="submitassignment" id="assignment_submit_button">Submit</button>
      </form>
    </div>

    </body>
    <script>
            $(document).ready(function () {
            $("#submitassignment").submit(function (e) {
                e.preventDefault();
                var formdata = new FormData();
                formdata.append("profile_photo", $("#profile_photo")[0].files[0])
                formdata.append("name", $("#name").val())
                formdata.append("age", $("#age").val())
                formdata.append("id", $("#id").val())
                console.log(formdata)

               $.ajax({
                    url: "/student/",
                    method: "PUT",
                    async: false,
                    cache: false,
                    contentType: false,
                    enctype: 'multipart/form-data',
                    processData: false,
                    data: formdata,
                    success: function (response) {
                    }
                })
            })
        })
    </script>
    </html>


.. code-block:: python

    @csrf_exempt
    def student_operation(request):
        if request.method == "GET":
            obj = Student.objects.all()
            return render(request, "datatables.html", {"obj": obj})
        elif request.method == "POST":
            name = request.POST.get("name")
            age = request.POST.get("age")
            profile_photo = request.FILES.get("profile_photo")
            obj = Student(name=name, age=age, profile_photo=profile_photo)
            obj.save()
            return HttpResponse("success")
        elif request.method == "DELETE":
            data = json.loads(request.body)
            id = data.get('id')
            obj = Student.objects.get(id=id)
            obj.delete()
            return HttpResponse("success")
        elif request.method == "PUT":
            pass
            # Need to do code here
