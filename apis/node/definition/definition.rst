
.. _node_api_definition:

=========================
**Node** definition
=========================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/Node





Definition
==========

The DOM Node interface is a **key base class** upon which many other
DOM API objects are based, thus letting those object types to be used
similarly and often interchangeably.

Key among the interfaces which inherit the features of Node are Document
and Element.

However, all of the following also inherit methods and properties from
Node:

- Attr,
- CharacterData (which Text, Comment, and CDATASection are all based on),
- ProcessingInstruction,
- DocumentFragment,
- DocumentType,
- Notation,
- Entity,
- and EntityReference.

In some cases, a particular feature of Node may not apply to a particular
interface based on it; in that case, the inheriting node may return null
or throw an exception, depending on circumstances.

For example, attempting to add children to a node type that cannot have
children will throw an exception.



Definition en français
==========================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/API/Node

Un Node (nœud) est une interface de laquelle un nombre d'objets de type
DOM API héritent ; il permet à ces différents types d'être traités de
manière similaire, par exemple en héritant du même ensemble de méthodes,
ou en étant testés de manière similaire.

Les interfaces suivantes héritent toutes des propriétés et méthodes de
Node :

- Document,
- Element,
- CharacterData (dans lequel Text, Comment, et CDATASection héritent),
- ProcessingInstruction,
- DocumentFragment,
- DocumentType,
- Notation,
- Entity,
- EntityReference

Ces interfaces peuvent retourner "null" dans certains cas particuliers
lorsque les méthodes ou propriétés ne sont pas pertinentes.

Elles peuvent aussi générer une exception - par exemple lors de l'ajout
d'un enfant à un type de node pour lequel aucun enfant ne peut exister.
