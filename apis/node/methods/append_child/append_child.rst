.. index::
   pair: Node ; appendChild
   ! appendChild

.. _append_child:

==============================
**Node element.appendChild**
==============================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/API/Node/appendChild


Description
============

La méthode Node.appendChild() ajoute un nœud à la fin de la liste des
enfants d'un nœud parent spécifié.

Si l'enfant donné est une référence à un nœud existant dans le document,
appendChild() le déplace  de sa position actuelle vers une nouvelle
position (il n'est pas nécessaire de supprimer le noeud sur son noeud
parent avant de l'ajouter à un autre).

Cela signifie qu'un noeud ne peut pas être à deux points du document
simultanément.
Donc, si le nœud a déjà un parent, le nœud est d'abord retiré, puis
ajouté à la nouvelle position.

Le Node.cloneNode() peut être utilisé pour réaliser une copie de noeud
avant de l'ajouter à son nouveau parent.

Notez que les copies faites avec cloneNode ne seront pas automatiquement
synchronisées.

Si l'enfant donné est un DocumentFragment , le contenu entier du
DocumentFragment est déplacé dans la liste d'enfants du noeud parent spécifié.


Examples
============

https://github.com/suzuki11109/vanilla-dnd
---------------------------------------------

.. seealso::

   - https://github.com/suzuki11109/vanilla-dnd


.. code-block:: javascript

    dropArea.addEventListener('drop', handleDrop, false);
    function handleDrop(e) {
      let dt = e.dataTransfer;
      let files = dt.files;

      handleFiles(files);
    }


    function handleFiles(files) {
      ([...files]).forEach(uploadFile);
      ([...files]).forEach(previewFile);
    }

    function uploadFile(file) {
      console.log(`${file} ${file.name} ${file.size} ${file.type}`);
    }

    function previewFile(file) {
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = function() {
        let img = document.createElement('img');
        img.src = reader.result;
        document.getElementById('gallery').appendChild(img);
      }
    }
