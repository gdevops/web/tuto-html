.. index::
   pair: Node ; methods

.. _node_methods:

=========================
**Node methods**
=========================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/API/Node


.. toctree::
   :maxdepth: 5

   append_child/append_child
