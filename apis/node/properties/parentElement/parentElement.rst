.. index::
   pair: Node ; parentElement
   pair: Table ; parentElement
   ! parentElement

.. _parent_element:

=========================
**Node.parentElement**
=========================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/API/Node/parentElement




Definition
===========

Returns an Element that is the parent of this node.

If the node has no parent, or if that parent is not an Element, this
property returns null.



Exemple
==========

.. code-block:: javascript
   :linenos:

    /**
     * Suppression d'une ligne lorsque le temps imputé === '00:00'
     *
     */
    function trt_temps_impute_0(value_temps_impute, element) {
        if (value_temps_impute === "00:00") {
            // https://gdevops.frama.io/web/linkertree/html/apis/element/element.html
            // https://gdevops.frama.io/web/linkertree/html/apis/element/table/table.html
            // https://gdevops.frama.io/web/linkertree/html/apis/HTMLTableElement/HTMLTableElement.html
            const table = document.getElementById('id_list_table');
            // https://gdevops.frama.io/web/linkertree/html/apis/node/properties/parentElement/parentElement.html
            // https://gdevops.frama.io/web/linkertree/html/apis/element/tr/tr.html
            const element_tr = element.parentElement.parentElement.parentElement;
            // https://gdevops.frama.io/web/linkertree/html/apis/HTMLTableRowElement/properties/rowIndex/rowIndex.html
            // https://gdevops.frama.io/web/linkertree/html/apis/HTMLTableElement/methods/deleteRow/deleteRow.html
            table.deleteRow(element_tr.rowIndex);
        }
    }

    function trt_temps_imputes() {
        console.log(`trt_temps_imputes() document.readyState = ${document.readyState}`);
        const url = '{% url "fiches_temps:ajax_update_fiche_temps" %}';
        const current_iso_week = document.getElementById('id_current_iso_week').innerHTML;
        const parameters = {'current_iso_week': current_iso_week};

        let value_temps_impute = null;
        {% for v, _, _ in listes_des_projets_fiches %}
            document.getElementById('id_fiches_temps_{{ v }}-temps_impute').onchange = function() {
              value_temps_impute = document.getElementById('id_fiches_temps_{{ v }}-temps_impute').value;
              parameters.temps_impute = get_temps_impute(value_temps_impute);
              parameters.fiche_temps_id = document.getElementById('id_fiches_temps_{{ v }}-id').value;
              parameters.projet_id = document.getElementById('id_fiches_temps_{{ v }}-projet').value;
              parameters.employe_id = document.getElementById('id_fiches_temps_{{ v }}-employe').value;
              parameters.created = document.getElementById('id_fiches_temps_{{ v }}-created').value;
              id3_fetch(url, parameters).then(json => {
                    document.getElementById('id_fiches_temps_{{ v }}-id').value= json.fiche_temps_id;
                    document.getElementById('id_fiches_temps_{{ v }}-created').value = json.created;
                    document.querySelector("#cumul_day").innerHTML = json.daily_hours_for_employe;

                    trt_temps_impute_0(value_temps_impute, this);
              });
            }
        {% endfor %}
    }
