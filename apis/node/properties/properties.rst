.. index::
   pair: Node ; properties

.. _node_properties:

=========================
**Node properties**
=========================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/API/Node


.. toctree::
   :maxdepth: 5

   parentElement/parentElement
