.. index::
   pair: Response ; Web API
   ! Response

.. _response_web_api:

=====================
**Response** Web API
=====================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/Response




Description
=============

The Response interface of the Fetch API represents the response to a request.

You can create a new Response object using the Response.Response() constructor,
but you are more likely to encounter a Response object being returned
as the result of another API operation—for example, a service worker
**Fetchevent.respondWith**, or a simple **GlobalFetch.fetch()**.


Définition en français
=======================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/API/Response

L'interface Response de l'API de fetch représente la réponse d'une
requête initialisée.

Vous pouvez créer un nouvel objet Response en utilisant le constructeur
Response.Response().

Cependant, vous rencontrerez plus fréquemment l'objet Response comme é
tant le résultat d'une opération de l'API, par exemple, un service worker
**Fetchevent.respondWith**, ou un simple **GlobalFetch.fetch()**.
