
.. _window_web_api_def:

==================================
**window** Web API definition
==================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/Window




Definition
=============

The **Window interface** represents a window containing a DOM document;

the document property points to the DOM document loaded in that window.

A window for a given document can be obtained using the document.defaultView
property.

A global variable, **window**, representing the window in which the script
is running, is exposed to JavaScript code.

The Window interface is home to a variety of functions, namespaces, objects,
and constructors which are not necessarily directly associated with the
concept of a user interface window.

However, the Window interface is a suitable place to include these items
that need to be globally available.
Many of these are documented in the JavaScript Reference and the DOM Reference.

In a tabbed browser, each tab is represented by its own Window object;
the global window seen by JavaScript code running within a given tab always
represents the tab in which the code is running.

That said, even in a tabbed browser, some properties and methods still
apply to the overall window that contains the tab, such as resizeTo()
and innerHeight.

Generally, anything that can't reasonably pertain to a tab pertains to
the window instead.


Définition en français
========================

L'objet window représente une fenêtre contenant un document DOM ;

la propriété document pointe vers le document DOM chargé dans cette
fenêtre. Une fenêtre pour un document donné peut être obtenue en
utilisant la propriété document.defaultView.

Cette section fournit une brève référence pour toutes les méthodes,
propriétés et événements disponibles via l'objet DOM window.

L'objet window implémente l'interface Window, qui à son tour hérite de
l'interface AbstractView.
Certaines fonctions globales supplémentaires, espaces de noms, objets,
interfaces et constructeurs, non typiquement associés à la fenêtre,
mais disponibles sur celle-ci, sont répertoriés dans la Référence
JavaScript et la Référence DOM.

Dans un navigateur utilisant des onglets, comme Firefox, chaque onglet
contient son propre objet window (et si vous écrivez une extension, la
fenêtre du navigateur elle-même est un objet window séparé — consultez
Travailler avec des fenêtres dans du code chrome pour plus d'informations).

C'est-à-dire que l'objet window n'est pas partagé entre les onglets
dans la même fenêtre.

Certaines méthodes, notamment window.resizeTo et window.resizeBy,
s'appliquent à la fenêtre entière et non à l'onglet spécifique auquel
l'objet window appartient.

En général, ce qui ne peut raisonnablement pas concerner un onglet se
rapporte à la fenêtre à la place.
