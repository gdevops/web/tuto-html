
.. _window_web_api_events:

==================================
**window** Web API events
==================================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/Events

.. toctree::
   :maxdepth: 3

   beforeunload/beforeunload
   load/load
