.. index::
   pair: Window; load
   pair: load; Event
   ! load

.. _window_load_event:

==================================
**load** event
==================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/Window/load_event
   - https://developer.mozilla.org/fr/docs/Web/Events/load
   - https://javascript.info/onload-ondomcontentloaded



Definition
===========

The **load** event is fired when the whole page has loaded, including all
dependent resources such as stylesheets and images.

This is in contrast to :ref:`DOMContentLoaded <DOMContentLoaded_event>`, which
is fired as soon as the page DOM has been loaded, without waiting for
resources to finish loading.
