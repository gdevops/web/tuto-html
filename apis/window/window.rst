.. index::
   pair: Window ; Web API
   ! Window

.. _window_web_api:

=====================
**window** Web API
=====================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/Window

.. toctree::
   :maxdepth: 3


   definition/definition
   events/events
