


.. _html_def:

=====================================
HTML definition
=====================================

- https://en.wikipedia.org/wiki/HTML5
- https://developer.mozilla.org/en-US/docs/Glossary/HTML5

HTML5 APIs and related_technologies
======================================

.. figure:: ../images/HTML5_APIs_and_related_technologies.png
   :width: 800
   :align: center

   By Mercury999 - Own work, CC BY-SA 4.0 https://commons.wikimedia.org/w/index.php?curid=36352535

   - https://en.wikipedia.org/wiki/HTML5


HTML5 Mozilla definition (MDN)
===============================

- https://developer.mozilla.org/en-US/docs/Glossary/HTML5

The term HTML5 is **essentially a buzzword** that refers to a set of
modern web technologies.
This includes the HTML Living Standard, along with JavaScript APIs to
enhance storage, multimedia, and hardware access.

You may sometimes hear about "new HTML5 elements", or find HTML5 described
as a new version of HTML.
HTML5 was the successor to previous HTML versions and introduced new
elements and capabilities to the language on top of the previous version,
HTML 4.01, as well as improving or removing some existing functionality.

However, as a Living Standard **HTML now has no version**.
The up-to-date specification can be found at https://html.spec.whatwg.org/

Any modern site should use the HTML doctype — this will ensure that you
are using the latest version of HTML.



W3C definition
================

.. seealso::

   - https://www.w3.org/html/

What is HTML ?
----------------

HTML is the Web’s core language for creating content for everyone to use
anywhere.

.. code-block:: html
   :linenos:

    <!DOCTYPE html>
    <html lang="en">
    <head>
    <title>Story</title>
    </head>
    <body>
    <h1>My Story</h1>
    <p>Once upon a time,
      …</p>
    </body>
    </html>

English wikipedia definition
==============================

.. seealso::

   - https://en.wikipedia.org/wiki/HTML

Hypertext Markup Language (HTML) is the standard markup language for creating
web pages and web applications.

With Cascading Style Sheets (CSS) and JavaScript, it forms a triad of cornerstone
technologies for the World Wide Web.

Web browsers receive HTML documents from a web server or from local storage
and render the documents into multimedia web pages.

HTML describes the structure of a web page semantically and originally
included cues for the appearance of the document.

HTML elements are the building blocks of HTML pages.

With HTML constructs, images and other objects such as interactive forms may be
embedded into the rendered page.
HTML provides a means to create structured documents by denoting structural
semantics for text such as headings, paragraphs, lists, links, quotes and
other items.

HTML elements are delineated by tags, written using angle brackets.
Tags such as <img /> and <input /> directly introduce content into the page.

Other tags such as <p> surround and provide information about document text
and may include other tags as sub-elements.

Browsers do not display the HTML tags, but use them to interpret the content
of the page.

HTML can embed programs written in a scripting language such as JavaScript,
which affects the behavior and content of web pages.

Inclusion of CSS defines the look and layout of content.
The World Wide Web Consortium (W3C), maintainer of both the HTML and the CSS
standards, has encouraged the use of CSS over explicit presentational
HTML since 1997

French wikipedia definition
==============================

- https://fr.wikipedia.org/wiki/HTML


L’HyperText Markup Language, généralement abrégé HTML, est le langage de
balisage conçu pour représenter les pages web.

C’est un langage permettant d’écrire de l’hypertexte, d’où son nom.

HTML permet également de structurer sémantiquement et logiquement et de
mettre en forme le contenu des pages, d’inclure des ressources multimédias
dont des images, des formulaires de saisie et des programmes informatiques.

Il permet de créer des documents interopérables avec des équipements très
variés de manière conforme aux exigences de l’accessibilité du web.

Il est souvent utilisé conjointement avec le langage de programmation
JavaScript et des feuilles de style en cascade (CSS).

HTML est inspiré du Standard Generalized Markup Language (SGML).
Il s'agit d'un format ouvert.

HTML5 French wikipedia definition (**28 octobre 2014**)
--------------------------------------------------------

- https://fr.wikipedia.org/wiki/HTML5


HTML5 (HyperText Markup Language 5) est la dernière révision majeure du
HTML (format de données conçu pour représenter les pages web).

Cette version a été finalisée **le 28 octobre 2014**.

HTML5 spécifie deux syntaxes d'un modèle abstrait défini en termes de DOM:

- HTML5
- et XHTML5.

Le langage comprend également :

- une couche application avec de nombreuses API
- un algorithme afin de pouvoir traiter les documents à la syntaxe non conforme.

Le travail a été repris par le W3C en mars 2007 après avoir été lancé
par le WHATWG.
Les deux organisations travaillent en parallèle sur le même document
afin de maintenir une version unique de la technologie.

Le W3C clôt les ajouts de fonctionnalités le 22 mai 2011, annonçant une
finalisation de la spécification en 2014, et encourage les développeurs
Web à utiliser HTML 5 dès ce moment.

Fin 2016, la version 5.1 est officiellement publiée et présente plusieurs
nouveautés qui doivent faciliter le travail des développeurs d'applications Web.



HTML Mozilla definition (MDN)
===============================

- https://developer.mozilla.org/en-US/docs/Web/HTML

HTML (HyperText Markup Language) is the most basic building block of the Web.
It defines the meaning and structure of web content.

Other technologies besides HTML are generally used to describe a web page's
appearance/presentation (CSS) or functionality/behavior (JavaScript).

"Hypertext" refers to links that connect web pages to one another, either
within a single website or between websites.

Links are a fundamental aspect of the Web.

By uploading content to the Internet and linking it to pages created by other
people, you become an active participant in the World Wide Web.

