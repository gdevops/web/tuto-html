.. index::
   pair: a; element
   ! a

.. _html_element_a:

=====================================
HTML **a** element
=====================================

- https://developer.mozilla.org/fr/docs/Web/HTML/Element/a

Description
============

L'élément HTML <a> (pour ancre ou anchor en anglais), avec son attribut
href, crée un lien hypertexte vers des pages web, des fichiers, des adresses e-mail,
des emplacements se trouvant dans la même page, ou tout ce qu'une URL
peut adresser.

Le contenu de chaque élément <a> doit indiquer la destination du lien.

Si l'attribut href est présent, appuyer sur la touche entrée en se
concentrant sur l'élément <a> l'activera.


Django template example
==========================

::

    <li><a href="{{ url_demande_mission_create }}" onclick="return confirm('Voulez-vous vraiment créer un ordre de mission ?');">Nouvelle demande ordre de mission</a></li>
