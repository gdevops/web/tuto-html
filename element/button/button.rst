.. index::
   pair: button; element
   ! button

.. _html_button:

=====================================
HTML **button** element
=====================================

- https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Button
- https://developer.mozilla.org/fr/docs/Web/HTML/Element/Button

Description
============

The <button> HTML element represents a clickable button, used to submit
forms or anywhere in a document for accessible, standard button functionality.

By default, HTML buttons are presented in a style resembling the platform
the user agent runs on, but you can change buttons’ appearance with CSS.


Description en français
==========================

- https://developer.mozilla.org/fr/docs/Web/HTML/Element/Button

:<button>: l'élément représentant un bouton

L'élément <button> représente un bouton cliquable, utilisé pour soumettre
des formulaires ou n'importe où dans un document pour une fonctionnalité
de bouton accessible et standard.

Par défaut, les boutons HTML sont présentés dans un style ressemblant à
la plate-forme d'exécution de l'agent utilisateur, mais vous pouvez modifier
l'apparence des boutons avec CSS.
