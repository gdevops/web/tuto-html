.. index::
   pair: data ; list
   pair: element ; data-list
   ! data-list

.. _data_list:

===============================================
**<datalist>: The HTML Data List element**
===============================================

- https://developer.mozilla.org/en-US/docs/Web/HTML/Element/datalist
- https://x.com/devsimplicity/status/1449313022166638598?s=20


You can use a native <datalist> element to create autocomplete dropdowns
without using JS.

Your markup will have a semantic meaning, better integration and better
accessibility.


::

    <label for="ice-cream-choice">Choose a flavor:</label>
    <input list="ice-cream-flavors" id="ice-cream-choice" name="ice-cream-choice" />

    <datalist id="ice-cream-flavors">
        <option value="Chocolate">
        <option value="Coconut">
        <option value="Mint">
        <option value="Strawberry">
        <option value="Vanilla">
    </datalist>
