.. index::
   pair: HTML; Element
   ! HTML Element

.. _html_element:

=====================================
**HTML element**
=====================================

- https://developer.mozilla.org/en-US/docs/Web/HTML/Element

.. toctree::
   :maxdepth: 3

   a/a
   button/button
   datalist/datalist
   iframe/iframe
   input/input
   select/select
   textarea/textarea
