
.. _moz_html_iframe:

===================================================
Mozilla HTML **iframe**
===================================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/HTML/Element/iframe
   - https://developer.mozilla.org/en-US/docs/Glossary/browsing_context



Description
=============

The HTML Inline Frame element (<iframe>) represents a `nested browsing`_
context, embedding another HTML page into the current one.

.. _`nested browsing`: https://developer.mozilla.org/en-US/docs/Glossary/browsing_context


Each embedded browsing context has its own session history and document.

The browsing context that embeds the others is called the parent
browsing context.

The topmost browsing context — the one with no parent — is usually
the browser window, represented by the **Window object**.
