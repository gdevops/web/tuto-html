.. index::
   pair: button; input
   ! input button

.. _html_input_button:

=====================================
HTML input **button** element
=====================================

- https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/button

Definition en français
=======================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/HTML/Element/Input/button


Les éléments <input> de type button sont affichés comme des boutons
poussoirs qui peuvent être programmés afin de contrôler des fonctionnalités
de la page via un gestionnaire d'évènement (la plupart du temps pour l'évènement click).


Examples
=========

.. seealso::

   - https://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_onclick_date

::

    <!DOCTYPE html>
    <html>
    <body>

    <p>Click the button to display the time.</p>

    <button onclick="getElementById('demo').innerHTML=Date()">What is the time?</button>

    <p id="demo"></p>

    </body>
    </html>
