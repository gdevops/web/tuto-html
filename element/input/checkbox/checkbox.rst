.. index::
   pair: HTML ; checkbox

.. _checkbox:

=====================================
HTML input **checkbox** element
=====================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/checkbox


.. toctree::
   :maxdepth: 3

   definition/definition
   examples/examples
