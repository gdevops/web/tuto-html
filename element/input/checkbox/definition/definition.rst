
.. _checkbox_def:

=====================================
HTML input checkbox definition
=====================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/checkbox
   - https://developer.mozilla.org/fr/docs/Web/HTML/Element/Input/checkbox




English definition
=====================

<input> elements of type checkbox are rendered by default as boxes that
are checked (ticked) when activated, like you might see in an official
government paper form.

The exact appearance depends upon the operating system configuration
under which the browser is running.

Generally this is a square but it may have rounded corners.

A checkbox allows you to select single values for submission in a form (or not).


Définition en français
========================

Les éléments <input> de type checkbox sont affichés sous la forme de
boîtes à cocher qui sont cochées lorsqu'elles sont activées.

Elle permettent de sélectionner une ou plusieurs valeurs dans un formulaire.
