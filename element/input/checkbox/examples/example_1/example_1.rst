
.. _django_checkbox_example_1:

==============================================
Django input checkbox example (ajax example)
==============================================



jQuery + vanilla JavaScript
===============================

.. code-block:: django
   :linenos:

    $('#id_etat_demande').change(function () {
        let etat_demande= $(this).val()
        let id_demande_article = document.getElementById('id_demande_article').textContent;
        let widget = document.getElementById('id_send_email');
        let send_email = widget.checked;
        // alert("id_demande_article=" + id_demande_article +" etat_demande=" + etat_demande);
        let url = '{% url "articles:ajax_update_demande_article" %}';
        $.ajax(
                {
                    url: url,
                    data: {
                        'id_demande_article': id_demande_article,
                        'etat_demande': etat_demande,
                        'send_email': send_email,
                    },
                    dataType: 'json',
                    success: function (data) {
                    }
            }
        );
    });



urls.py
=========

::

    path(
        "ajax/demande_article_update",
        view_update_demande_article,
        name="ajax_update_demande_article",
    ),


articles_update.html
======================

.. literalinclude:: articles_update.html
   :language: django
   :linenos:


views_demande_article.py
==========================

.. literalinclude:: views_demande_article.py
   :linenos:
