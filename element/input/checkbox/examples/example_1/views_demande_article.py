"""Vues concernant les articles et demandes d'articles.

Techniquement on utilise les vues génériques

- https://docs.djangoproject.com/en/dev/ref/class-based-views/generic-display/


"""
import logging
import time
from typing import List, Union

import pendulum

from django.http.request import HttpRequest
from django.http import HttpResponseRedirect, JsonResponse

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required

from django.core.paginator import EmptyPage, Page, PageNotAnInteger, Paginator
from django.http import HttpResponse, HttpResponseRedirect
from django.http.request import HttpRequest, QueryDict
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse, reverse_lazy
from django.views.generic.base import RedirectView

# https://docs.djangoproject.com/en/dev/ref/class-based-views/generic-display/#detailview
from django.views.generic.detail import DetailView

# https://docs.djangoproject.com/en/dev/ref/class-based-views/generic-editing/#updateview
from django.views.generic.edit import CreateView, UpdateView

# https://docs.djangoproject.com/en/dev/ref/class-based-views/generic-display/#listview
# http://ccbv.co.uk/projects/Django/1.9/django.views.generic.list/ListView/
from django.views.generic.list import ListView

from documents.views import parse_date
from employes.decorators import require_authenticated_permission
from employes.models import Employe, StatusEmploye
from projets.models import Projet

from .forms_demande_article import (
    DemandeArticleCreateForm,
    DemandeArticleFormFiltre,
    DemandeArticleUpdateForm,
)
from .models_demande_article import (
    ETAT_DEMANDE_ARTICLE_CHOICES,
    DemandeArticle,
    ListdisplayDemandeArticle,
)

from django.contrib import messages

# Get an instance of a logger
logger = logging.getLogger(__name__)


__all__ = (
    "DemandeArticleNonClotureeListView",
    "DemandeArticleCreateView",
    "DemandeCodifArticleUpdateView",
    "DemandeCodifNomenclatureUpdateView",
    "DemandeAchatServiceUpdateView",
    "DemandeSortieArticleUpdateView",
    "view_update_demande_article",
)


class DemandeArticleRedirectView(RedirectView):

    permanent = False
    query_string = True
    pattern_name = "articles:demande_achat_service_update"

    def get_redirect_url(self, *args, **kwargs):
        demande_article = get_object_or_404(DemandeArticle, pk=kwargs["pk"])
        # logger.info(f"DemandeArticleRedirectView() demande_article={demande_article} permanent={DemandeArticleRedirectView.permanent}")
        if demande_article.demande_codification_par_article():
            DemandeArticleRedirectView.pattern_name = (
                "articles:demande_codif_article_update"
            )
        elif demande_article.demande_codification_par_nomenclature():
            DemandeArticleRedirectView.pattern_name = (
                "articles:demande_codif_nomenclature_update"
            )
        elif demande_article.demande_codification_achat_par_service():
            DemandeArticleRedirectView.pattern_name = (
                "articles:demande_achat_service_update"
            )
        elif demande_article.demande_codification_sortie_stock():
            DemandeArticleRedirectView.pattern_name = (
                "articles:demande_sortie_article_update"
            )

        return super().get_redirect_url(*args, **kwargs)


class DemandeArticleMixin:
    @property
    def success_msg(self):
        return NotImplemented

    def get_context_data(self, **kwargs):
        """
        - http://gravitymad.com/blog/2012/4/14/how-filter-django-forms-multiple-choice-or-foreign/
        """
        context = super().get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        messages.info(self.request, self.success_msg)
        return super().form_valid(form)


class DemandeArticleNonClotureeListView(LoginRequiredMixin, ListView):
    """Affiche la liste des demandes d'articles non cloturées.

    - on n'affiche pas les demandes cloturees
    - affichage des dernières demandes (tri decroissant sur id)

    """

    queryset = (
        DemandeArticle.objects.exclude(
            etat_demande=ETAT_DEMANDE_ARTICLE_CHOICES.Cloturee
        )
        .order_by("-id")
        .select_related("demandeur")
        .prefetch_related(
            "codifarticle_related",
            "achatservice_related",
            "sortiestock_related",
            "codifnomenclature_related",
        )
    )

    template_name = "articles/demande/list_non_cloturee.html"
    context_object_name = "demandes_articles"


@require_authenticated_permission("add_demandearticle")
class DemandeArticleCreateView(DemandeArticleMixin, CreateView):
    """Affiche la vue permettant la création d'une demande d'articles.

    SeeAlso:

    - http://ccbv.co.uk/projects/Django/1.9/django.views.generic.edit/CreateView/

    """

    model = DemandeArticle
    form_class = DemandeArticleCreateForm
    template_name = "articles/demande/create.html"
    success_msg = "La demande d'article a bien été créé !"

    def form_valid(self, form):
        """
        voir p.523 Mastering Django core
        """
        try:
            # logger.info(f"User={self.request.user}")
            employe = Employe.objects.get(user=self.request.user)
            # Le demandeur est l'employé connecté
            form.instance.demandeur = employe
            return super().form_valid(form)
        except Exception as error:
            logger.error(f"Employe {self.request.user} not found:{error}")
            messages.error(self.request, error)
            return None

    def get_success_url(self):
        """Retourne l'URL à afficher après une création réussie d'une demande d'article.

        Depuis le lundi 24 février 2020, on affiche directement le premier article
        de la demande.

        """
        demande_article = self.object
        # update_url = demande_article.get_update_url()
        # on crée un premier enregistrement
        try:
            demande_article.create_first_article()
        except Exception as error:
            logger.error(f"Exception={error}")

        update_url = demande_article.get_update_url_for_articles()
        # logger.info(f"DemandeArticleCreateView.get_success_url() {demande_article=}\n{update_url=}")

        return update_url


@require_authenticated_permission("change_demandearticle")
class DemandeAchatServiceUpdateView(UpdateView):
    """Affiche la vue permettant de modifier une demande d'article par un service.

    SeeAlso:

    - http://ccbv.co.uk/projects/Django/1.9/django.views.generic.edit/UpdateView/

    """

    model = DemandeArticle
    context_object_name = "demande_article"
    form_class = DemandeArticleUpdateForm
    template_name = "articles/demande/achat_service/update.html"
    success_msg = "La demande d'article a bien été mise à jour !"

    def get_success_url(self):
        return reverse(
            "articles:demande_achat_service_update", kwargs={"pk": self.object.pk}
        )


@require_authenticated_permission("change_demandearticle")
class DemandeCodifArticleUpdateView(UpdateView):
    """Affiche la vue permettant de modifier une demande de codification par article.

    SeeAlso:

    - http://ccbv.co.uk/projects/Django/1.9/django.views.generic.edit/UpdateView/

    """

    model = DemandeArticle
    context_object_name = "demande_article"
    form_class = DemandeArticleUpdateForm
    template_name = "articles/demande/codif_article/update.html"
    success_msg = "Demande article mis à jour !"

    def get_success_url(self):
        return reverse(
            "articles:demande_codif_article_update", kwargs={"pk": self.object.pk}
        )


@require_authenticated_permission("change_demandearticle")
class DemandeCodifNomenclatureUpdateView(UpdateView):
    """Affiche la vue permettant de modifier une demande de codification nomenclature.

    SeeAlso:

    - http://ccbv.co.uk/projects/Django/1.9/django.views.generic.edit/UpdateView/

    """

    model = DemandeArticle
    context_object_name = "demande_article"
    form_class = DemandeArticleUpdateForm
    template_name = "articles/demande/codif_nomenclature/update.html"
    success_msg = "Demande article mis à jour !"

    def get_success_url(self):
        return reverse(
            "articles:demande_codif_nomenclature_update", kwargs={"pk": self.object.pk}
        )


@require_authenticated_permission("change_demandearticle")
class DemandeSortieArticleUpdateView(UpdateView):
    """Affiche la vue permettant de modifier une demande de sortie d'article .

    SeeAlso:

    - http://ccbv.co.uk/projects/Django/1.9/django.views.generic.edit/UpdateView/

    """

    model = DemandeArticle
    context_object_name = "demande_article"
    form_class = DemandeArticleUpdateForm
    template_name = "articles/demande/sortie_stock/update.html"
    success_msg = "Demande article mis à jour !"

    def get_success_url(self):
        return reverse(
            "articles:demande_sortie_article_update", kwargs={"pk": self.object.pk}
        )


class DemandeArticleListView(LoginRequiredMixin, ListView):
    """Affiche la liste des demandes d'articles.

    - on affiche toutes les demandes
    - affichage des dernières demandes (tri decroissant sur id)

    """

    queryset = (
        DemandeArticle.objects.exclude(
            etat_demande=ETAT_DEMANDE_ARTICLE_CHOICES.Cloturee
        )
        .order_by("-id")
        .select_related("demandeur")
        .prefetch_related(
            "codifarticle_related",
            "achatservice_related",
            "sortiestock_related",
            "codifnomenclature_related",
        )
    )

    template_name = "articles/demande/list.html"
    context_object_name = "demandes_articles"


def ReadGetVueDemandeArticleList(
    request: HttpRequest,
) -> (
    pendulum.datetime,
    pendulum.datetime,
    int,
    int,
    Employe,
    str,
    int,
    Projet,
    DemandeArticleFormFiltre,
):
    # GET: c'est quand on pointe sur la page directement ou que l'on
    # utilise paginator
    # logger.info(f"Begin ReadGetVueDemandeArticleList() request.GET={request.GET}")
    start_time = time.time()

    # Est-ce qu'il y a un filtre sur la date from_date ?
    from_date = parse_date(request.GET.get("from_date"))
    if from_date is None:
        # from_date = pendulum.now("Europe/Paris").add(months=-12)
        from_date = pendulum.datetime(1990, 4, 1, tz="Europe/Paris")

    # Est-ce qu'il y a un filtre sur la date to_date ?
    to_date = parse_date(request.GET.get("to_date"))
    if to_date is None:
        # la date du jour
        to_date = pendulum.now("Europe/Paris").add(hours=12)
        # logger.info(f"to_date:{to_date}")

    try:
        type_demande = request.GET.get("type_demande")
        if type_demande != "":
            type_demande = int(type_demande)
        else:
            type_demande = None
    except Exception as error:
        type_demande = None

    try:
        etat_demande = request.GET.get("etat_demande")
        if etat_demande != "":
            etat_demande = int(etat_demande)
        else:
            etat_demande = None

    except Exception as error:
        etat_demande = None

    try:
        demandeur = request.GET.get("demandeur")
        if demandeur != "":
            id_demandeur = int(demandeur)
            demandeur = Employe.objects.get(pk=id_demandeur)
        else:
            demandeur = None
    except Exception as error:
        demandeur = None

    try:
        description = request.GET.get("description")
    except Exception as error:
        description = None

    try:
        id_demande = request.GET.get("id_demande")
        if id_demande == "":
            id_demande = None
    except Exception as error:
        id_demande = None

    try:
        projet = request.GET.get("projet")
        if projet != "":
            id_projet = int(projet)
            # on s'en sert pour récupérer le type de document
            projet = Projet.objects.get(pk=id_projet)
        else:
            projet = None

    except Exception as error:
        projet = None

    # Instanciation du formulaire de recherche.
    # https://docs.djangoproject.com/en/dev/ref/forms/api/#dynamic-initial-values
    formulaire_filtre_demandes_articles = DemandeArticleFormFiltre(
        initial={
            "from_date": from_date,
            "to_date": to_date,
            "description": description,
            "filtre_description": False
            if description is None or description == ""
            else True,
            "id_demande": id_demande if id_demande is not None else "",
            "filtre_id_demande": False if id_demande is None else True,
            "type_demande": type_demande if type_demande is not None else "",
            "filtre_type_demande": False if type_demande is None else True,
            "etat_demande": etat_demande if etat_demande is not None else "",
            "filtre_etat_demande": False if etat_demande is None else True,
            "demandeur": demandeur.id if demandeur is not None else "",
            "filtre_demandeur": False if demandeur is None else True,
            "projet": projet.id if projet is not None else "",
            "filtre_projet": False if projet is None else True,
        }
    )

    # logger.info(
    #    f"ReadGetVueDemandeArticleList() formulaire_filtre_demandes_articles:{formulaire_filtre_demandes_articles}"
    # )

    end_time = time.time()
    duree = end_time - start_time
    logger.info(f"ReadGetVueDemandeArticleList() DUREE:<{duree}s>")

    return (
        from_date,
        to_date,
        type_demande,
        etat_demande,
        demandeur,
        description,
        id_demande,
        projet,
        formulaire_filtre_demandes_articles,
    )


def ReadPostVueDemandeArticleList(
    request: HttpRequest,
) -> (
    pendulum.datetime,
    pendulum.datetime,
    int,
    int,
    Employe,
    str,
    int,
    Projet,
    DemandeArticleFormFiltre,
):
    # POST c'est quand l'utilisateur appuie sur le bouton 'Appliquer'
    # A revoir car "Search forms that are idempotent should use the GET method"
    # p. 142, chapitre 11 "Form fundamentals" Two scoops of Django
    # logger.info("POST={}".format(request.POST))
    type_demande = None
    etat_demande = None
    demandeur = None
    description = ""
    id_demande = None
    projet = None
    if "btn_form_filtre" in request.POST:
        # Instanciation du formulaire
        form_filtres_demandes_articles = DemandeArticleFormFiltre(request.POST)
        if form_filtres_demandes_articles.is_valid():
            # indispensable pour avoir le tableau 'cleaned_data'
            # qui remet les dates anglaises en place:
            # Exemple
            date_naive = form_filtres_demandes_articles.cleaned_data["from_date"]
            from_date = pendulum.datetime(
                date_naive.year, date_naive.month, date_naive.day, tz=("Europe/Paris")
            )
            date_naive = form_filtres_demandes_articles.cleaned_data["to_date"]
            to_date = pendulum.datetime(
                date_naive.year, date_naive.month, date_naive.day, tz=("Europe/Paris")
            )
            filtre_type_demande = form_filtres_demandes_articles.cleaned_data[
                "filtre_type_demande"
            ]
            if filtre_type_demande:
                # recuperation de l'id de la table type_demande
                type_demande = int(
                    form_filtres_demandes_articles.cleaned_data["type_demande"]
                )

            filtre_etat_demande = form_filtres_demandes_articles.cleaned_data[
                "filtre_etat_demande"
            ]
            if filtre_etat_demande:
                # recuperation de l'id de la table etat_demande
                etat_demande = int(
                    form_filtres_demandes_articles.cleaned_data["etat_demande"]
                )

            filtre_demandeur = form_filtres_demandes_articles.cleaned_data[
                "filtre_demandeur"
            ]
            if filtre_demandeur:
                # recuperation de l'id de la table Demandeur
                id_demandeur = int(
                    form_filtres_demandes_articles.cleaned_data["demandeur"]
                )
                demandeur = Employe.objects.get(pk=id_demandeur)

            filtre_description = form_filtres_demandes_articles.cleaned_data[
                "filtre_description"
            ]
            if filtre_description:
                # recuperation de la désignation
                description = form_filtres_demandes_articles.cleaned_data["description"]

            filtre_id_demande = form_filtres_demandes_articles.cleaned_data[
                "filtre_id_demande"
            ]
            if filtre_id_demande:
                id_demande = int(
                    form_filtres_demandes_articles.cleaned_data["id_demande"]
                )
            else:
                id_demande = None

            filtre_projet = form_filtres_demandes_articles.cleaned_data["filtre_projet"]
            if filtre_projet:
                # recuperation de l'id de la table Projet
                id_projet = int(form_filtres_demandes_articles.cleaned_data["projet"])
                projet = Projet.objects.get(pk=id_projet)

        else:
            logger.info(f"Form is NOT valid {form_filtres_demandes_articles}")

    return (
        from_date,
        to_date,
        type_demande,
        etat_demande,
        demandeur,
        description,
        id_demande,
        projet,
        form_filtres_demandes_articles,
    )


def get_page_and_paginator(
    request: HttpRequest,
    liste_demandes_articles: List[DemandeArticle],
    NB_ELEMS_IN_PAGE=50,
) -> (Page, Paginator):

    # mise à jour de l'objet paginator
    # https://docs.djangoproject.com/en/dev/topics/pagination/
    paginator = Paginator(liste_demandes_articles, NB_ELEMS_IN_PAGE, orphans=3)
    # Quelle est la page à afficher
    page = request.GET.get("page")
    try:
        current_page = paginator.page(page)
    except PageNotAnInteger:
        current_page = paginator.page(1)
    except EmptyPage:
        current_page = paginator.page(paginator.num_pages)

    return current_page, paginator


def get_liste_demandes_articles(
    from_date,
    to_date,
    type_demande: int,
    etat_demande: int,
    demandeur: Employe,
    description: str,
    id_demande: int,
    projet: Projet,
) -> List[DemandeArticle]:
    """Lecture des DemandeArticles en fonction des critères."""
    # a partir du jour précédent la date de sélection
    # logger.info("Begin get_liste_demandes_articles()")

    """
    logger.info(
        f"\nBegin get_liste_demandes_articles()\n"
        f"from_date:{from_date}\n"
        f"to_date:{to_date}\n"
        f"description:{description}\n"
        f"id_demande:'{id_demande}'\n"
        f"type_demande:{type_demande}\n"
        f"etat_demande:{etat_demande}\n"
        f"demandeur:{demandeur}\n"
        f"projet:{projet}\n"
    )
    """

    # cas 1: on connait l'identifiant de la demande
    if id_demande is not None:
        try:
            liste_demandes_articles = DemandeArticle.objects.filter(id=id_demande)
            return liste_demandes_articles
        except Exception as error:
            logger.error(f"Exception: {error}")

    # start_time = time.time()
    liste_demandes_articles = DemandeArticle.objects.filter(
        created__range=(from_date, to_date)
    ).select_related("demandeur")

    # end_time = time.time(); duree = end_time - start_time
    # logger.info(f"uDemandeArticle.objects.filter({from_date} {to_date}): DUREE:<{duree}s>")

    # cas 2 : sélection sur un projet
    if projet is not None:
        try:
            # logger.info(f"Projet:{projet}")
            set_demandes = set()
            # sélection des différents articles liés au projet
            for article in projet.achatservice_related.all():
                set_demandes.add(article.demande_article.id)

            for article in projet.codifarticle_related.all():
                set_demandes.add(article.demande_article.id)

            for article in projet.codifnomenclature_related.all():
                set_demandes.add(article.demande_article.id)

            for article in projet.sortiestock_related.all():
                set_demandes.add(article.demande_article.id)

            # filtre sur les ids des demandes
            # pour constitution d'un query set
            liste_demandes_articles = liste_demandes_articles.filter(
                pk__in=list(set_demandes)
            )
        except Exception as error:
            logger.error(f"Exception: {error}")

    # cas 3
    # filtres normaux
    try:
        if type_demande is not None:
            # selection d'une agence particulière
            liste_demandes_articles = liste_demandes_articles.filter(
                type_demande=type_demande
            )
        if etat_demande is not None:
            liste_demandes_articles = liste_demandes_articles.filter(
                etat_demande=etat_demande
            )
        if demandeur is not None:
            liste_demandes_articles = liste_demandes_articles.filter(
                demandeur=demandeur
            )
        if description is not None:
            # recherche description = on recherche dans la description
            liste_demandes_articles = liste_demandes_articles.filter(
                description__icontains=description
            )
    except Exception as error:
        logger.error(f"Exception: {error}")

    return liste_demandes_articles


@login_required
def DemandeArticleList(
    request: HttpRequest, template_name="articles/demande/list.html"
) -> HttpResponse:
    """Affichage des DemandeArticles (codes-chronos)."""
    logger.info("Begin DemandeArticleList()")

    user = request.user
    form_filtres_demandes_articles = None
    if request.method == "POST":
        (
            from_date,
            to_date,
            type_demande,
            etat_demande,
            demandeur,
            description,
            id_demande,
            projet,
            form_filtres_demandes_articles,
        ) = ReadPostVueDemandeArticleList(request)
    elif request.method == "GET":
        (
            from_date,
            to_date,
            type_demande,
            etat_demande,
            demandeur,
            description,
            id_demande,
            projet,
            form_filtres_demandes_articles,
        ) = ReadGetVueDemandeArticleList(request)

    # lecture de la liste des DemandeArticle
    liste_demandes_articles = get_liste_demandes_articles(
        from_date,
        to_date,
        type_demande,
        etat_demande,
        demandeur,
        description,
        id_demande,
        projet,
    )
    # logger.info(f"liste_demandes_articles:{liste_demandes_articles}")

    # mise à jour de l'objet paginator
    current_page, paginator = get_page_and_paginator(request, liste_demandes_articles)
    nb_demandes_articles = liste_demandes_articles.count()

    # logger.info(f"paginator={paginator} num_pages:{paginator.num_pages}")
    multiple_page = False
    # if current_page.count() > 1:
    #    multiple_page = True

    # logger.info(f"nb_demandes_articles={nb_demandes_articles}")

    return render(
        request,
        template_name,
        context={
            "form_filtres_demandes_articles": form_filtres_demandes_articles,
            "user": user,
            "nb_demandes_articles": nb_demandes_articles,
            "demandes_articles": liste_demandes_articles,
            "from_date": "0" if from_date is None else from_date,
            "to_date": "0" if to_date is None else to_date,
            "type_demande": type_demande if type_demande is not None else "",
            "etat_demande": etat_demande if etat_demande is not None else "",
            "demandeur": demandeur if demandeur is not None else "",
            "description": description if description is not None else "",
            "id_demande": id_demande if id_demande is not None else "",
            "projet": projet if projet is not None else "",
            "paginator": paginator,
            "current_page": current_page,
        },
    )


def get_change_for_etat_demande(
    send_email: bool, current_demande: DemandeArticle, nouvel_etat_demande: int
) -> Union[str, None]:
    """S courriel à envoyer et etat_demande On indique le changement d'état dans le courriel envoyé

    Si pas de courriel à envoyer message_change = None
    """
    if not send_email:
        return None

    if current_demande.etat_demande == nouvel_etat_demande:
        return None

    message_change = (
        f"Changement par rapport à la dernière version:\n"
        f"=============================================\n"
        f"\n"
    )
    try:
        message_change = message_change + (
            f"Etat_demande:\n"
            f"-------------\n"
            f"{ETAT_DEMANDE_ARTICLE_CHOICES.for_value(current_demande.etat_demande).display} "
            f"=> "
            f"{ETAT_DEMANDE_ARTICLE_CHOICES.for_value(nouvel_etat_demande).display}"
            f"\n"
        )
    except Exception as error:
        logger.error(f"{error=}")

    return message_change


def get_change_for_description(
    send_email: bool, current_demande: DemandeArticle, nouvelle_description: str
) -> Union[str, None]:
    """S courriel à envoyer et nouvelle_description != description courante
    on indique le changement d'état.
    desz

    Si pas de courriel à envoyer message_change = None
    """
    if not send_email:
        return None

    if current_demande.description == nouvelle_description:
        return None

    if current_demande.description != nouvelle_description:
        message_change = (
            f"Changement par rapport à la dernière version:\n"
            f"=============================================\n"
            f"\n"
        )
        message_change = message_change + (
            f"Description:\n"
            f"------------\n"
            f"\n"
            f"{current_demande.description} "
            f"\n=>\n"
            f"{nouvelle_description}\n"
            f"\n"
        )

    return message_change


def view_update_demande_article(request: HttpRequest) -> JsonResponse:
    """Mise à jour de la demande d'article par AJAX.
    """
    id_demande_article = int(request.GET.get("id_demande_article", None))
    try:
        demande_article = DemandeArticle.objects.get(pk=id_demande_article)
    except Exception as error:
        logger.error(f"{error=}")

    description = request.GET.get("description", None)
    etat_demande = request.GET.get("etat_demande", None)
    send_email = request.GET.get("send_email", None)
    if send_email is not None:
        if send_email == "false":
            send_email = False
        elif send_email == "true":
            send_email = True

    else:
        send_email = False
    logger.info(f"{send_email=}")

    data = {}
    data["update"] = False
    message_change = ""
    logger.info(f"{demande_article=}")
    try:
        save = False
        msg = ""
        if description is not None:
            description = description.strip()
            logger.info(f"{description=}")
            changement = get_change_for_description(
                send_email, demande_article, description
            )
            if changement is not None:
                message_change = message_change + "\n" + changement
            demande_article.description = description
            msg = f"new description:{description}"
            save = True

        if etat_demande is not None:
            etat_demande = int(etat_demande)
            logger.info(f"{etat_demande=}")
            changement = get_change_for_etat_demande(
                send_email, demande_article, etat_demande
            )
            if changement is not None:
                message_change = message_change + "\n" + changement
            demande_article.etat_demande = etat_demande
            msg = f"new etat_demande:{etat_demande}"
            save = True

        if save:
            demande_article.save(message_change=message_change, send_email=send_email)
            data["update"] = msg
            # retour data
            if description is not None:
                data["libelle_description"] = description

    except Exception as error:
        logger.error(f"view_update_demande_article {error=}")

    return JsonResponse(data)
