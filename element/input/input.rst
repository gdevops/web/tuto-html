
.. index::
   pair: HTML; input
   ! input


.. _html_input_element:

=====================================
HTML **input** element
=====================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input

.. toctree::
   :maxdepth: 3

   button/button
   checkbox/checkbox
   file/file
