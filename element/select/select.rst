.. index::
   pair: HTML; select
   ! select


.. _html_select_element:

=====================================
HTML **select** element
=====================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/HTML/Element/select
   - https://developer.mozilla.org/fr/docs/Web/HTML/Element/Select




Définition en français
========================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Web/HTML/Element/Select


L'élément HTML <select> représente un contrôle qui fournit une liste
d'options parmi lesquelles l'utilisateur pourra choisir.
