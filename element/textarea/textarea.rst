
.. index::
   pair: textarea ; element
   ! textarea

.. _html_textarea_element:

=====================================
HTML textarea element
=====================================


- https://developer.mozilla.org/en-US/docs/Web/HTML/Element/textarea
- https://developer.mozilla.org/en-US/docs/Web/API/HTMLTextAreaElement


Description
==============

The HTML <textarea> element represents a multi-line plain-text editing
control, useful when you want to allow users to enter a sizeable amount
of free-form text, for example a comment on a review or feedback form.


HTMLTextAreaElement
=======================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/API/HTMLTextAreaElement


The HTMLTextAreaElement interface provides special properties and methods
for manipulating the layout and presentation of <textarea> elements.


Autogrowing textareas
==========================

- https://gomakethings.com/autogrowing-textareas/
- https://css-tricks.com/the-cleanest-trick-for-autogrowing-textareas/


Three years ago, I shared an approach for creating autoexpanding textarea
elements with vanilla JS. On a recent project, I decided to search for
a more modern way to automatically expand textarea elements as the user
types, and stumbled upon `this solution <https://css-tricks.com/the-cleanest-trick-for-autogrowing-textareas/>` (https://css-tricks.com/the-cleanest-trick-for-autogrowing-textareas/)
from Stephen Shaw that I think is far superior.

