.. index::
   pair: Evolution; openui
   ! openui

.. _openui:

===========================================================================================
**openui** (Maintain an open standard for UI and promote its adherence and adoption)
===========================================================================================

- https://github.com/openui
- https://github.com/openui/open-ui
- https://open-ui.org/
- https://x.com/openuicg


.. figure:: images/logo_openui.png
   :width: 200


datepicker
============

- https://open-ui.org/components/datepicker.research


select
=======

- https://open-ui.org/components/select.research


tabs
====

- https://open-ui.org/components/tabs.research
- https://bkardell.com/blog/SpicySections
- https://daverupert.com/2021/10/native-html-tabs/
