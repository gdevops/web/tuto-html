.. index::
   pair: HTML; Global attributes
   ! Global attributes

.. _html_global_attributes:

=====================================
**HTML global attributes**
=====================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes

.. toctree::
   :maxdepth: 3

   style/style
