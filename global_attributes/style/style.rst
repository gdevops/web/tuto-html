.. index::
   ! style

.. _html_style:

=====================================
**HTML global style attribute**
=====================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/style




Definition
===========

The style global attribute contains CSS styling declarations to be applied
to the element.

.. note:: it is recommended for styles to be defined in a separate file  or files.
   This attribute and the <style> element have mainly the purpose of allowing
   for quick styling, for example for testing purposes



Définition en français
=========================

L'attribut universel style contient des déclarations CSS afin de mettre
en forme l'élément.

.. warning:: il est recommandé de définir les règles de mise en forme
  dans un ou plusieurs fichiers séparés.

  Cet attribut, ainsi que l'élément <style> ont simplement pour but de
  permettre une mise en forme rapide, notamment pour tester.
