.. index::
   pair: HTML ; Howto

.. _html_howto:

=====================================
**Howto**
=====================================

- https://developer.mozilla.org/en-US/docs/Learn/HTML/Howto


.. toctree::
   :maxdepth: 3

   use_data_attributes/use_data_attributes
