
.. _howto_data_attributes:

=====================================
**Using data attributes**
=====================================

- https://developer.mozilla.org/en-US/docs/Learn/HTML/Howto/Use_data_attributes
- https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/data-\*

HTML is designed with extensibility in mind for data that should be
associated with a particular element but need not have any defined
meaning.

`data-\* attributes <https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/data-\*>`_
allow us to store extra information on standard, semantic HTML elements
without other hacks such as non-standard attributes, or extra properties on DOM.

