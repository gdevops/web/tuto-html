.. index::
   ! HyperText Markup Language
   ! HTML



.. figure:: images/2023/02/html_living_standard.png
   :align: center

   2023-02-23, https://html.spec.whatwg.org/

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>

|FluxWeb| `RSS <https://gdevops.frama.io/web/tuto-html/rss.xml>`_


.. _html:
.. _html_standard:
.. _html_tuto:
.. _tuto_html:

===========================================================
**HTML living standard** (HyperText Markup Language)
===========================================================

- https://github.com/whatwg/html
- https://github.com/whatwg/html/commits/main
- https://github.com/whatwg/html/graphs/contributors
- https://html.spec.whatwg.org/
- https://x.com/htmlstandard
- https://developer.mozilla.org/en-US/docs/Glossary/HTML5
- https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML
- https://en.wikipedia.org/wiki/HTML
- https://developer.mozilla.org/en-US/docs/Web/HTML
- https://w3c.github.io/html/
- https://www.w3.org/html/


.. warning:: The W3C ceded authority over the HTML and DOM standards to
   WHATWG on 28 May 2019, as it considered that having two standards is
   harmful.
   **The HTML Living Standard is now authoritative**.
   However, W3C will still participate in the development process of HTML.

   - https://en.wikipedia.org/wiki/HTML5#W3C_and_WHATWG_conflict
   - https://www.w3.org/blog/2019/05/w3c-and-whatwg-to-work-together-to-advance-the-open-web-platform/


.. note:: HTML5 The term HTML5 is essentially a buzzword that refers to
   a set of modern web technologies.
   This includes the HTML Living Standard, along with JavaScript APIs to
   enhance storage, multimedia, and hardware access.

   You may sometimes hear about "new HTML5 elements", or find HTML5
   described as a new version of HTML.
   HTML5 was the successor to previous HTML versions and introduced new
   elements and capabilities to the language on top of the previous version,
   HTML 4.01, as well as improving or removing some existing functionality.
   However, as a Living Standard HTML now has no version.
   The up-to-date specification can be found at html.spec.whatwg.org/

   - https://developer.mozilla.org/en-US/docs/Glossary/HTML5
   - https://html.spec.whatwg.org/dev/introduction.html#is-this-html5?



.. toctree::
   :maxdepth: 6

   definition/definition
   apis/apis
   global_attributes/global_attributes
   element/element
   howto/howto
   inline_elements/inline_elements
   tips/tips
   tools/tools
   evolution/evolution
   glossary/glossary
   news/news
   tutorials/tutorials
   versions/versions
