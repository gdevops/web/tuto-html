.. index::
   pair: HTML; Inline Elements
   ! Inline Elements

.. _html_inline_elements:

=====================================
**HTML inline elements**
=====================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/HTML/Inline_elements

.. toctree::
   :maxdepth: 3
