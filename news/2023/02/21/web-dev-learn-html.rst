
.. _ref_learn_html:

===========================================================
2023-02-21 **Web dev learn HTML available**
===========================================================

- https://web.dev/learn/html/
- :ref:`web_dev_learn_html`


Announce (2023-02-21)
==========================

- https://web.dev/learn-html-available/

All modules in the Learn HTML course are now available.

This course was written by Estelle Weyl (https://front-end.social/@estelle)  and takes you through an in-depth
journey to really understand HTML.


I've been writing HTML for a very long time, and I learned things while
editing this course.
There really is a lot more to HTML than you might think, and it's definitely
worth revisiting some of the things you think you already know.
Take a look, and don't forget to check out some of the other courses in
our growing collection.

