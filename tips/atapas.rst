
.. _atapas_html_tips:

===========================================================================================================
10 useful HTML5 features, you may not be using by Tapas Adhikary on Aug 14, 2020
===========================================================================================================

.. seealso::

   - https://blog.greenroots.info/10-useful-html5-features-you-may-not-be-using-ckdua7ql300l1m3s1ez7teshc
   - https://github.com/atapas/html-tips-tricks
   - https://x.com/tapasadhikary
   - https://github.com/atapas
   - https://tapasadhikary.com/



Introduction
==============


HTML5 is not a new thing.

We have been using several features of it since the initial release (January 2008).

As part of #100DaysOfCode initiative, I have taken a close look to the
HTML5 feature list again. See what I found? I haven't really used a bunch
of it so far!

In this article, I am listing down ten such HTML5 features that I haven't
used much in past but, found them useful now. I have also created a
working example flow and hosted on netlify. Hope you find it useful too.

::

    html5-tips.netlify.app

Great, so let us get started with the explanation, code and quick tips
about each of them.


Details Tag
=============

.. seealso::

   - https://html5-tips.netlify.app/details/index.html

Description
------------

The <details> tag provides on demand details to the user.

If you have a need to show content to the user on demand, use this tag.
By default, the widget is closed.

When open, it expands, and displays the content within.

The <summary> tag is used with <details> to specify a visible heading for it.


Code
------

.. code-block:: html

    <details>
         <summary>Click Here to get the user details</summary>
             <table>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Location</th>
                        <th>Job</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Adam</td>
                        <td>Huston</td>
                        <td>UI/UX</td>
                    </tr>
              </table>
      </details>



Quick Tips
--------------

Use it in GitHub Readme for showing the detailed information on demand.

Here is an example of how I have hidden a huge list of react component
properties and show it only on demand. Cool, right?



Content Editable
==================

.. seealso::

   - https://html5-tips.netlify.app/content-editable/index.html


contenteditable is an attribute that can be set on an element to make
the content editable.

It works with elements like, DIV, P, UL etc. You have to specify it
like, <element contenteditable="true|false">

Code
------

.. code-block:: html


    <h2> Shoppping List(Content Editable) </h2>
    <ul class="content-editable" contenteditable="true">
        <li> 1. Milk </li>
        <li> 2. Bread </li>
        <li> 3. Honey </li>
    </ul>


Quick Tips
--------------

A span or div elements can be made editable with it and you can add any
rich content to it using css styling.

This will be way better than handling it with input fields. Give it a try!


Map
=====

.. seealso::

   - https://html5-tips.netlify.app/map/index.html

Description
------------

The <map> tag helps in defining an image map.

An image map is any image with one or more clickable areas within it.
The map tag goes with an <area> tag to determine the clickable areas.
The clickable areas could be either of these shapes, rectangle, circle
or polygonal region. If you do not specify any shape, it considers the
entire image.

Code
------

.. code-block:: html

    <div>
        <img src="circus.jpg" width="500" height="500" alt="Circus" usemap="#circusmap">

        <map name="circusmap">
            <area shape="rect" coords="67,114,207,254" href="elephant.htm">
            <area shape="rect" coords="222,141,318, 256" href="lion.htm">
            <area shape="rect" coords="343,111,455, 267" href="horse.htm">
            <area shape="rect" coords="35,328,143,500" href="clown.htm">
            <area shape="circle" coords="426,409,100" href="clown.htm">
        </map>
    </div>

Tips
------

Image map has its own drawbacks but, you can use it for visual presentations.

How about trying it out with a family photo and drill down into individual's
photo(may be the old ones we always cherish for!).


Mark Content
====================

.. seealso::

   - https://html5-tips.netlify.app/mark/index.html


Description
------------

Use the <mark> tag to highlight any text content.

Code
------

.. code-block:: html

   <p> Did you know, you can <mark>"Highlight something interesting"</mark> just with a HTML tag? </p>


Tips
-----

You can always change the highlight color using css,

.. code-block:: css

    mark {
      background-color: green;
      color: #FFFFFF;
    }

data-\* attribute
=======================

.. seealso::

   - https://html5-tips.netlify.app/data-attribute/index.html

Description
------------

The data-\* attributes are used to store custom data private to the page
or application.

The stored data can be used in JavaScript code to create further **user experiences**.

The data-\* attributes consist of two parts:

- The attribute name should not contain any uppercase letters, and must
  be at least one character long after the prefix "data-"
- The attribute value can be any string


Code
------

.. code-block:: html

     <h2> Know data attribute </h2>
     <div
           class="data-attribute"
           id="data-attr"
           data-custom-attr="You are just Awesome!">
           I have a hidden secret!
      </div>

      <button onclick="reveal()">Reveal</button>

Then in JavaScript

.. code-block:: javascript

    function reveal() {
       let dataDiv = document.getElementById('data-attr');
       let value = dataDiv.dataset['customAttr'];
       document.getElementById('msg').innerHTML = `<mark>${value}</mark>`;
    }


.. note:: For reading the values of these attributes in JavaScript,
   you could use getAttribute() with their full HTML name(i.e, data-custom-attr)
   but, the standard defines a simpler way: using a dataset property.


HTML page
------------

.. code-block:: html

    <!DOCTYPE html>
    <html>

    <head>
        <meta charset='utf-8'>
        <meta http-equiv='X-UA-Compatible' content='IE=edge'>
        <title>HTML Tips and Tricks - Data Attribute</title>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <link href="https://fonts.googleapis.com/css2?family=Chilanka&display=swap" rel="stylesheet">
        <link rel='stylesheet' type='text/css' media='screen' href='../main.css'>

        <script>
            function reveal() {
                let dataDiv = document.getElementById('data-attr');
                let value = dataDiv.dataset['customAttr'];
                document.getElementById('msg').innerHTML = `<mark>${value}</mark>`;
            }

        </script>
    </head>

    <body>
        <div class="demo">
            <a href="../index.html" class="home">
                <img src="../home.svg" alt="home" />
            </a>
            <h2> Know data attribute </h2>
            <div
                class="data-attribute"
                id="data-attr"
                data-custom-attr="You are just Awesome!"> I have a hidden secret!
            </div>
            <button onclick="reveal()">Reveal</button>

            <br />

            <p id="msg"></p>
        </div>
    </body>

    </html>

Quick Tips
--------------

You can use it to store some data in the page and then pass it using
REST call to the server.

Another use-case could be the way, I show a `notification message count here <https://github.com/atapas/notifyme/blob/17a8e66e3bfacad4a32cd5c50bf1d6f6cc3fc863/NotifyMe.js#L147>`_.


Output Tag
===========

.. seealso::

   - https://html5-tips.netlify.app/output/index.html

Description
---------------

The <output> tag represents the result of a calculation.

Typically this element defines a region that will be used to display
text output from some calculation.

Code
-----

.. code-block:: html

    <form oninput="x.value=parseInt(a.value) * parseInt(b.value)">
       <input type="number" id="a" value="0">
              * <input type="number" id="b" value="0">
                    = <output name="x" for="a b"></output>
    </form>

HTML page
------------

.. code-block:: html

    <!DOCTYPE html>
    <html>

        <head>
            <meta charset='utf-8'>
            <meta http-equiv='X-UA-Compatible' content='IE=edge'>
            <title>HTML Tips and Tricks - Output</title>
            <meta name='viewport' content='width=device-width, initial-scale=1'>
            <link href="https://fonts.googleapis.com/css2?family=Chilanka&display=swap" rel="stylesheet">
            <link rel='stylesheet' type='text/css' media='screen' href='../main.css'>
        </head>

        <body>
            <div class="demo">
                <a href="../index.html" class="home">
                    <img src="../home.svg" alt="home" />
                </a>
                <h2> Let's see the Output </h2>
                <form oninput="x.value=parseInt(a.value) * parseInt(b.value)">
                    <input type="number" id="a" value="0">
                    * <input type="number" id="b" value="0">
                    = <output name="x" for="a b"></output>
                </form>
            </div>
        </body>
    </html>

Tips
------

If you are performing any computation in the client side JavaScript and,
want the result to reflect on the page, use <output> tag.

**You do not have to walk the extra steps of getting an element using getElementById()**.

Datalist
=========

.. seealso::

   - https://html5-tips.netlify.app/datalist/index.html

Description
---------------

The <datalist> tag specifies a list of pre-defined options and allows
user to add more to it.

It provides an autocomplete feature that allows you to get the desired
options with a **type-ahead**.

Code
-----

.. code-block:: html

    <form action="" method="get">
        <label for="fruit">Choose your fruit from the list:</label>
        <input list="fruits" name="fruit" id="fruit">
            <datalist id="fruits">
               <option value="Apple">
               <option value="Orange">
               <option value="Banana">
               <option value="Mango">
               <option value="Avacado">
            </datalist>
         <input type="submit">
    </form>

Tips
------

How is it different than the traditional <select>-<option> tag?

Select tag is for selecting one or more items from the options where,
you need to go through the list to pick from.

Datalist is the advanced feature with an autocomplete support.


**Range (Slider)**
====================

.. seealso::

   - https://html5-tips.netlify.app/range/index.html

Description
---------------

The range is an input type given a slider kind of range selector.

Code
-----

.. code-block:: html


     <form method="post">
        <input
             type="range"
             name="range"
             min="0"
             max="100"
             step="1"
             value=""
             onchange="changeValue(event)"/>
     </form>
     <div class="range">
          <output id="output" name="result">  </output>
     </div>


HTML page
------------

.. code-block:: html


    <!DOCTYPE html>
    <html>

    <head>
        <meta charset='utf-8'>
        <meta http-equiv='X-UA-Compatible' content='IE=edge'>
        <title>HTML Tips and Tricks - Range Slider</title>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <link href="https://fonts.googleapis.com/css2?family=Chilanka&display=swap" rel="stylesheet">
        <link rel='stylesheet' type='text/css' media='screen' href='../main.css'>

        <script>
            function changeValue(event) {
                let value = event.target.value;
                let output = document.getElementById('output');
                output.value = value;
            }
        </script>
    </head>

    <body>
        <div class="demo">
            <a href="../index.html" class="home">
                <img src="../home.svg" alt="home" />
            </a>
            <h1>Slider</h1>
            <form method="post">
                <input
                    type="range"
                    name="range"
                    min="0"
                    max="100"
                    step="1"
                    value=""
                    onchange="changeValue(event)"/>
            </form>
            <div class="range">
                <output id="output" name="result">  </output>
            </div>
        </div>
    </body>

    </html>


Meter
======

.. seealso::

   - https://html5-tips.netlify.app/meter/index.html

Description
---------------

Use the <meter> tag to measure data within a given range.

Code
-----

.. code-block:: html

    <label for="home">/home/atapas</label>
    <meter id="home" value="4" min="0" max="10">2 out of 10</meter><br>

    <label for="root">/root</label>
    <meter id="root" value="0.6">60%</meter><br>


HTML page
-----------

.. code-block:: html


    <!DOCTYPE html>
    <html>

        <head>
            <meta charset='utf-8'>
            <meta http-equiv='X-UA-Compatible' content='IE=edge'>
            <title>HTML Tips and Tricks - Meter</title>
            <meta name='viewport' content='width=device-width, initial-scale=1'>
            <link href="https://fonts.googleapis.com/css2?family=Chilanka&display=swap" rel="stylesheet">
            <link rel='stylesheet' type='text/css' media='screen' href='../main.css'>
        </head>

        <body>
            <div class="demo">
                <a href="../index.html" class="home">
                    <img src="../home.svg" alt="home" />
                </a>
                <h2> Meter </h2>
                <label for="home">/home/atapas</label>
                <meter id="home" value="4" min="0" max="10">2 out of 10</meter><br>

                <label for="root">/root</label>
                <meter id="root" value="0.6">60%</meter><br>

                <label for="file">Downloading progress:</label>
                <progress id="file" value="32" max="100"> 32% </progress>
            </div>
        </body>
    </html>


Tips
------

Do not use the <meter> tag for a **progress indicator kind of user experience**.

We have the <Progress> tag from HTML5 for it.

.. code-block:: html

    <label for="file">Downloading progress:</label>
    <progress id="file" value="32" max="100"> 32% </progress>


Inputs
=========

This part is mostly known to us with the usage of input types like, text,
password etc. There are few special usage of the input types,


required
-------------

Mark an input field as mandatory.

.. code-block:: html

    <input type="text" id="username1" name="username" required>


autofocus
-----------

Provides focus to the input element automatically by placing the cursor
on it.

.. code-block:: html

   <input type="text" id="username2" name="username" required autofocus>

pattern: validation with regex
---------------------------------

You can specify a pattern using regex to validate the input.

.. code-block:: html

    <input type="password"
                name="password"
                id="password"
                placeholder="6-20 chars, at least 1 digit, 1 uppercase and one lowercase letter"
                pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$" autofocus required>

color: Color picker
-----------------------

A simple color picker.

.. code-block:: html

   <input type="color" onchange="showColor(event)">
   <p id="colorMe">Color Me!</p>



What's next ?
==============

Well, I am sure, I have left behind few useful ones.

How about you complete the list? Please provide comments about this post
and your learning on HTML5. See you soon with my next article.

Oh Yes, all the code used in this article can be found in the `git repo
mentioned below <https://tapasadhikary.com/>`_.

Please give the repo a star, if you liked the work.
