.. index::
   pair: Hide ; Element
   ! Hide

.. _how_to_hide_element:

================================
How to hide Web page elements
================================

.. seealso::

   - https://dev.to/ziizium/how-to-hide-web-page-elements-1be5
   - https://a11yproject.com/posts/how-to-hide-content/
   - https://davidwalsh.name/html5-hidden




Introduction
===============

When you have a Web page there is possibility that you want to hide
some page elements if some conditions are met. These conditions can include:

- Successful load of the page.
- JavaScript files has successfully downloaded.
- JavaScript is enabled.
- Change in device orientation.

Whatever situation you might find yourself in, it's best to hide the
page elements with **accessibility in mind**.

How to hide page elements
==========================

Multiple options are available in your tool set as a developer when it
comes to hiding page elements.

Some include:

- HTML style attribute.
- CSS display property.
- CSS position property.
- Hiding elements with JavaScript.


HTML style attribute
========================

When you use HTML style attribute to hide page elements you'll have to
use it with some CSS properties.

Most of the time that will be the display property with a value of none.

::

    <p style="display: none;">This text is not shown on screen</p>

You should know that when you hide page elements with the display: none;
it is ignored by screen readers. e.g. NVDA.

CSS display property
========================

CSS display property with a value of none can be used in a number of
ways depending on the condition. It can be used in the following cases:

    Hide a page element totally
    Changes in device orientation

Hide a page element totally
-------------------------------

The following selector will hide the page element totally and the Web
browser will not render it on screen and a screen reader won't read it either.

::

    .selector {
        display: none;
    }

Make sure that any element you hide totally is not critical to understanding
your content nor decrease its accessibility.

Hiding the search input on mobile is not really a good thing unless you
have a mechanism to reveal it and the mechanism itself is accessible.


Changes in device orientation
=================================

When you are designing with responsive web design approach you will find
yourself hiding stuff on mobile and showing them on tablet or desktop view.

Take the following example::

    .left-side-bar {
        display: none;
    }

    /**
     * On tablet view upwards we show the
     * the left side bar
     */
    @media screen and (min-width: 48em) {
        .left-side-bar {
            display: block;
        }
    }


CSS position property
========================

.. seealso::

   - https://dev.to/ziizium/how-to-implement-accessibility-skip-to-content-4d8m

The position property can be employed to position Web page elements just
about where you want them on a Web page.

It can also be used to move page element totally off-screen.

I detailed how to do this in my post entitled: `How to implement accessibility "skip to content"`_.

The following will move the selected page element off-screen but it is
still available to screen readers::

    .visually-hidden {
        position: absolute;
        top: auto;
        left: -10000px;
        width: 1px;
        height: 1px;
        overflow: hidden;
    }


.. _`How to implement accessibility "skip to content"`:  https://dev.to/ziizium/how-to-implement-accessibility-skip-to-content-4d8m


Hiding elements with JavaScript
====================================

Hiding page elements with JavaScript is two folds. They are:

- Directly applying inline styles.
- Adding a class name that will hide the elements.

Directly applying inline styles
-----------------------------------

Web page methods can be selected using a number DOM methods like
document.getElementByID(), document.querySelector() e.t.c.

After this you can apply styling that will hide the element directly to it.

Take a look the following example::

    const header = document.getElementById('header');

    // hide the header
    header.style.display = "none";

You should know that the header in the code above will only get hidden
when the user has JavaScript enabled in their browser or their Web browser
has successfully downloaded your JavaScript files.

Adding a class name that will hide the elements
--------------------------------------------------

You can create a class in your CSS files that will exclusively for
hiding page elements.

Then, you can select a page element with JavaScript and add this class to it.

The following CSS class hides the element that it gets applied to::

    .hidden {
        display: none;
    }

Then you can hide the desired page element by adding the .hidden class
to its class attribute::

    const footNote= document.getElementByID('foot-note');

    // add the hidden class
    footNote.classList.add('hidden');

Now that we've highlighted some ways to hide page elements there are
some things you should avoid doing.

Things you should avoid doing
=================================

Do not hide element with CSS with the intention of showing it with
JavaScript. This is bad.

Do not do this::

    .left-side-bar {
        display: none;
    }

Then showing it with JavaScript::

    const leftSideBar = document.querySelector('.left-side-bar');

    // then show it
    leftSideBar.style.display = "block";

If the user has JavaScript disabled or the JavaScript fails to download
the user will lose access to the element.

f you need to do this make sure it's not a critical element of your page
like an archive sidebar that links to your previous blog post(s).

Real life example
===================

In the image below, I am using DEV basic markdown editor but as you will
notice you can see the Publish and Save draft button which are available
in the rich editor.


These buttons are later hidden when the JavaScript downloads.
Had the DEV team hid the buttons with CSS so that it gets shown with
JavaScript, these buttons will not be shown and you might see empty spaces.

Conclusion
================

There are many options when it comes to hiding page elements, the
choice is yours but remember to do it with accessibility in mind.

Edit June 25, 2020: Add link to article about accessibility "skip to content".
