.. index::
   pair: display ; property
   ! htmldom.dev


.. _web_htmldom_dev:

================================
https://htmldom.dev/
================================

.. seealso::

   - https://htmldom.dev/
   - https://x.com/nghuuphuoc





https://htmldom.dev/show-or-hide-an-element
================================================

Show an element
--------------------

::

    ele.style.display = '';

Hide an element
-----------------

::

    ele.style.display = 'none';


https://htmldom.dev/toggle-an-element
========================================

To toggle the element, we update the **display property**::

    const toggle = function(ele) {
        const display = ele.style.display;
        ele.style.display = display === 'none' ? 'block' : 'none';
    };
