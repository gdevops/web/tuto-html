.. index::
   ! thisthat.dev

.. _thisthat:

================================
https://thisthat.dev/
================================

.. seealso::

   - https://thisthat.dev/
   - https://github.com/phuoc-ng/this-vs-that
   - https://x.com/nghuuphuoc
