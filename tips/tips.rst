.. index::
   pair: HTML; Tips
   ! HTML Tips

.. _html_tips:

=====================================
**HTML tips**
=====================================

.. toctree::
   :maxdepth: 3

   atapas
   how_to_hide_web_page_elements
   htmldom.dev
   html_css_js.com
   thisthat.dev
