.. index::
   pair: MDN; play


.. _mdn_play:

===========================================================================================================
**MDN play**
===========================================================================================================

- https://developer.mozilla.org/en-US/play
- :ref:`mdn_play_2023_06_22`


.. figure:: images/mdn_play.png
   :align: center



Under the hood : https://codemirror.net/
=============================================

- https://codemirror.net/

We decided to go with `CodeMirror <https://codemirror.net/>`_ for editing.

As they phrase it::

    CodeMirror is a code editor component for the web. It can be used in
    websites to implement a text input field with support for many editing
    features, and has a rich programming interface to allow further extension.

We considered `Monaco <https://microsoft.github.io/monaco-editor/>`_, but decided for a more lightweight approach.
So far it feels like the right choice, let's wait for feedback.
