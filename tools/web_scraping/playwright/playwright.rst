.. index::
   ! playwright

.. _playwright:

===========================================================================================================
**playwright** a framework for Web Testing **and Automation**
===========================================================================================================

- https://x.com/playwrightweb
- https://github.com/microsoft/playwright
- https://github.com/microsoft/playwright/graphs/contributors
- https://x.com/aslushnikov
- https://playwright.dev/
- https://playwright.dev/blog

Specifications
==============

- Any browser • Any platform • One API
- Cross-browser. Playwright supports all modern rendering engines
  including Chromium, WebKit, and Firefox.
- Cross-platform. Test on Windows, Linux, and macOS, locally or on CI,
  headless or headed.
- Cross-language. Use the Playwright API in TypeScript, JavaScript,
  Python, .NET, Java.
- Test Mobile Web. Native mobile emulation of Google Chrome for Android
  and Mobile Safari. The same rendering engine works on your Desktop
  and in the Cloud.


Powerful Tooling
====================

- Codegen. Generate tests by recording your actions. Save them into any language.
- Playwright inspector. Inspect page, generate selectors, step through
  the test execution, see click points, explore execution logs.
- Trace Viewer. Capture all the information to investigate the test failure.
  Playwright trace contains test execution screencast, live DOM snapshots,
  action explorer, test source, and many more.
