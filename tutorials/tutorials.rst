.. index::
   pair: HTML; Tutorials

.. _html_tutorials:

===========================================================
**Tutorials**
===========================================================

.. toctree::
   :maxdepth: 3

   devdocs/devdocs
   web_dev_learn_html/web_dev_learn_html
