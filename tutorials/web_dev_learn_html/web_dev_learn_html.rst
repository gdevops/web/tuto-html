.. index::
   pair! HTML; Tutorials
   ! Estelle Weyl

.. _web_dev_learn_html:

===========================================================
**Web dev Learn HTML**
===========================================================

- https://web.dev/learn/html/


Estelle Weyl
================

- https://estelle.github.io/
- https://front-end.social/@estelle
- https://github.com/estelle
- https://codepen.io/estelle

.. figure:: images/estelle_github.png
   :align: center

   https://github.com/estelle/estelle


My name is Estelle. A little about me:


- 🔭 I work with [@openwebdocs](https://github.com/openwebdocs) on maintaining [@MDN](https://github.com/mdn),
- 📝 I am author of [Learn HTML](https://web.dev/learn/html) and co-author of [CSS: The Definitive Guide](https://learning.oreilly.com/library/view/css-the-definitive/9781098117603/)
- 🌱 I’m always learning CSS, HTML, JavaScript, and accessibility
- 👯 I’m looking to collaborate on projects that help reduce inequality in STEM and society as a whole.
- 🤔 I’m looking for help maintaining MDN
- 💬 Ask me about what you can do to be more inclusive.
- 📫 How to reach me: myFirstName@myLastName.org
- 😄 Pronouns: She/her
- ⚠️ I value kindness over niceties
- ⚡ Fun fact: I have a public health degree. Oddly, it comes in handy as a web developer, technical writer, and professional cat herder.

You can find me on <a rel="me" href="https://front-end.social/@estelle">Mastodon</a>. I am no longer on Twitter.




Announce (2023-02-21)
==========================

- https://web.dev/learn-html-available/
- https://front-end.social/@estelle/109906152573658956

All modules in the Learn HTML course are now available.

This course was written by Estelle Weyl (https://front-end.social/@estelle)  and takes you through an in-depth
journey to really understand HTML.


I've been writing HTML for a very long time, and I learned things while
editing this course.
There really is a lot more to HTML than you might think, and it's definitely
worth revisiting some of the things you think you already know.
Take a look, and don't forget to check out some of the other courses in
our growing collection.


accessibility
---------------

- https://front-end.social/@estelle/109903758911648650

Yes, the 20-part Learn HTML series has no article titled #Accessibility
because accessibility isn't a separate topic.

The 20-article learn HTML is a 20-section series on learning HTML,
which is accessible by default. Our job as developers is to not fuck
that up!
Teaching you how to not fuck up accessibility, and rather ensure accessibility,
is covered in Every Single Section of the series because it is the core of HTML.

https://web.dev/learn/html/


I wrote a thing
----------------

- https://front-end.social/@estelle/109903715577572699


I should add, if you think you really know HTML, I wrote a thing:
https://web.dev/learn/html/

Turns out there might be a lot more to HTML than you might have thought.
I learned (and shared) a ton of nuggets along the way, and diligently
updated MDN in the process.

#10XEngineer 😜

Overview
=============

This HTML course for web developers provides a solid overview for developers,
from novice to expert level HTML.

If you're completely new to HTML, you will learn how to build structurally
sound content.

If you've been building websites for years, this course may fill in gaps
in knowledge that you didn't even know you had.


Along this journey, we will be building the structure for MachineLearningWorkshop.com.
No machines were harmed in the creation of this series.

This is not a complete reference.

Each section introduces the section topic with brief explanations and
examples, providing you an opportunity to explore further.

There will be links to topic references, such as MDN and WHATWG specifications,
and other web.dev articles. While this is not an accessibility course,
each section will include accessibility best practices and specific issues,
with links to deeper dives on the topic.

Each section will have a short assessment to help people confirm their
understanding.

