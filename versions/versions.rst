.. index::
   pair: HTML ; Versions
   pair: HTML ; noversion

.. _html_versions:

=================================================================================
HTML5 versions => However, as a Living Standard, HTML, *now*, has **no version**
=================================================================================

- https://github.com/whatwg/html
- https://github.com/whatwg/html/issues
- https://github.com/whatwg/html/commits
- https://github.com/whatwg/html/graphs/contributors
- https://html.spec.whatwg.org/

:source:  https://developer.mozilla.org/en-US/docs/Glossary/HTML5

However, as a **Living Standard**, HTML, now, has **no version**.

The up-to-date specification can be found at https://html.spec.whatwg.org/.

Any modern site should use the HTML doctype — this will ensure that you
are using the latest version of HTML.


.. toctree::
   :maxdepth: 6

   5.2/5.2
   5.0/5.0
